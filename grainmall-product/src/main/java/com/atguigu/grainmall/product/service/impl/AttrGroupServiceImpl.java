package com.atguigu.grainmall.product.service.impl;

import com.atguigu.grainmall.product.entity.AttrEntity;
import com.atguigu.grainmall.product.service.AttrService;
import com.atguigu.grainmall.product.vo.AttrGroupWithAttrsVo;
import com.atguigu.grainmall.product.vo.SpuItemAttrGroupVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.grainmall.product.dao.AttrGroupDao;
import com.atguigu.grainmall.product.entity.AttrGroupEntity;
import com.atguigu.grainmall.product.service.AttrGroupService;
import org.springframework.util.StringUtils;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    AttrService attrService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询属性分组列表
     * @param params
     * @param catelogId
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        // 有id就按照指定id进行查询 判断如果携带了 封装中的 查询 key 值 则需要进行条件+属性id查询
        String key = (String) params.get("key");
        // 数据通用写法
        LambdaQueryWrapper<AttrGroupEntity> wrapper = Wrappers.lambdaQuery();
        if (!StringUtils.isEmpty(key)){
            wrapper.and((obj)->{
                // 是否 满足其中一个查询条件
                obj.eq(AttrGroupEntity::getAttrGroupId,key).or().like(AttrGroupEntity::getAttrGroupName,key);
            });
        }
        // 如果传过来的三级分类id=0 那么就查询所有 否则按照指定id进行查找
        if (catelogId == 0){
            // 调用分装好的工具类 page(查询分页条件，全部查询条件)
            IPage<AttrGroupEntity>  iPage = this.page(new Query<AttrGroupEntity>().getPage(params),
                    wrapper);
            // 封装好的方法 传入iPage 会自动解析 封装返回规定好的数据分页格式
            return new PageUtils(iPage);
        }else {
            wrapper.eq(AttrGroupEntity::getCatelogId,catelogId);
            // 调用分装好的工具类 page(查询分页条件，全部查询条件)
            IPage<AttrGroupEntity>  iPage = this.page(new Query<AttrGroupEntity>().getPage(params),
                    wrapper);
            // 封装好的方法 传入iPage 会自动解析 封装返回规定好的数据分页格式
            return new PageUtils(iPage);
        }
    }

    /**
     * 根据分类id获取分类下所有分组的关联属性
     * @param catelogId
     * @return
     */
    @Override
    public List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId) {
        // 查询分组信息
        List<AttrGroupEntity> groupEntityList = this.list(new LambdaQueryWrapper<AttrGroupEntity>().eq(AttrGroupEntity::getCatelogId, catelogId));
        // 查询所有属性
        List<AttrGroupWithAttrsVo> attrGroupWithAttrsVos = groupEntityList.stream().map((item) -> {
            AttrGroupWithAttrsVo attrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(item, attrsVo);
            // 属性信息
            List<AttrEntity> attrs = attrService.getRelationAttr(attrsVo.getAttrGroupId());
            attrsVo.setAttrs(attrs);
            return attrsVo;
        }).collect(Collectors.toList());

        return attrGroupWithAttrsVos;
    }

    /**
     * 获取spu的规格参数
     * @return
     */
    @Override
    public List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId) {
        // 查询出spu对应的分组信息以及当前分组下的所有的属性值
        List<SpuItemAttrGroupVo> vos = baseMapper.getAttrGroupWithAttrsBySpuId(spuId,catalogId);

        return vos;
    }

}
