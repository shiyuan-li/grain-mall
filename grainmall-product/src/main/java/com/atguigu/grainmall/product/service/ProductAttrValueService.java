package com.atguigu.grainmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:22
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存spu的规格参数-pms_product_attr_value
     * @param valueEntityList
     */
    void saveProductAttr(List<ProductAttrValueEntity> valueEntityList);

    /**
     * 获取spu规格
     * @param spuId
     * @return
     */
    List<ProductAttrValueEntity> baseAttrListForSpu(Long spuId);

    /**
     * 修改商品规格
     * @param spuId
     * @param entities
     * @return
     */
    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities);
}

