package com.atguigu.grainmall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @Author shiyuan
 * @Date 2023/2/26 9:50
 * @Version 1.0
 * Redisson程序化配置
 **/
@Configuration
public class MyRedissonConfig {

    /**
     * 所有对Redisson的操作都是通过Redisson对象来操作的 集群模式
     *
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson() throws IOException {
        // 创建配置
        Config config = new Config();
        // 配置节点模式与链接地址
        config.useSingleServer().setAddress("redis://192.168.56.10:6379");
        // 根据Config创建出RedissonClient的实例
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }

}
