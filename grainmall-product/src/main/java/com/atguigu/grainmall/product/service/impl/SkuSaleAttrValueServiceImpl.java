package com.atguigu.grainmall.product.service.impl;

import com.atguigu.grainmall.product.vo.SkuItemSaleAttrsVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.grainmall.product.dao.SkuSaleAttrValueDao;
import com.atguigu.grainmall.product.entity.SkuSaleAttrValueEntity;
import com.atguigu.grainmall.product.service.SkuSaleAttrValueService;


@Service("skuSaleAttrValueService")
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueDao, SkuSaleAttrValueEntity> implements SkuSaleAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuSaleAttrValueEntity> page = this.page(
                new Query<SkuSaleAttrValueEntity>().getPage(params),
                new QueryWrapper<SkuSaleAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 获取spu的销售属性组合
     * @param spuId
     * @return
     */
    @Override
    public List<SkuItemSaleAttrsVo> getSaleAttrsBySpuId(Long spuId) {
        SkuSaleAttrValueDao valueDao = this.baseMapper;
        List<SkuItemSaleAttrsVo> vos = valueDao.getSaleAttrsBySpuId(spuId);

        return null;
    }

    /**
     * 获取商品属性值信息
     * @param skuId
     * @return
     */
    @Override
    public List<String> getSkuSaleAttrValueAsStringList(Long skuId) {
        SkuSaleAttrValueDao valueDao = this.baseMapper;

        return valueDao.getSkuSaleAttrValueAsStringList(skuId);
    }

}
