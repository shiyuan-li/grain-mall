package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.vo.AttrGroupRelationVo;
import com.atguigu.grainmall.product.vo.AttrResponseVo;
import com.atguigu.grainmall.product.vo.AttrVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);


    /**
     * 保存
     */
    void saveAttr(AttrVo attr);

    /**
     * 获取分类的属性参数 进行分页查询  与 销售属性列表 以请求过俩的 Type 参数进行区分 是1 为销售属性 0 为分类属性
     *
     * @param param
     * @param catelogId
     * @param type
     */
    PageUtils queryBaseList(Map<String, Object> param, Long catelogId, String type);

    /**
     * 获取属性信息详情
     */
    AttrResponseVo getAttrIofo(Long attrId);

    /**
     * 修改
     * @param attr
     */
    void updateAttr(AttrVo attr);

    /**
     * 根据分组id获取关联的所有基本属性
     * @param attrgroupId
     * @return
     */
    List<AttrEntity> getRelationAttr(Long attrgroupId);

    /**
     * 删除属性与分组的关联关系
     * @param vos
     * @return
     */
    void deleteRelation(AttrGroupRelationVo[] vos);

    /**
     * 获取属性分组没有关联的其他属性
     * @param params
     * @param attrgroupId
     * @return
     */
    PageUtils getRelationNoAttr(Map<String, Object> params, Long attrgroupId);

    /**
     * 查询当前sku得所有可以被检索的规格属性
     * @param attrIds
     * @return
     */
    List<Long> selectSearchAttrIds(List<Long> attrIds);
}

