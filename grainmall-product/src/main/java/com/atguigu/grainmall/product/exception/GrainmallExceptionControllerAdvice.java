package com.atguigu.grainmall.product.exception;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author shiyuan
 * @Date 2023/1/27 15:01
 * @Version 1.0
 * 异常集中处理类
 **/
@Slf4j
//@ResponseBody // 以json形式返回所以使用 ResponseBody
//@ControllerAdvice(basePackages = "com.atguigu.grainmall.product.controller")  // 异常统一处理注解 并配置处理那个位置出现的异常
@RestControllerAdvice(basePackages = "com.atguigu.grainmall.product.controller") // 相当于 @ResponseBody 与 @ControllerAdvice 的合体使用
public class GrainmallExceptionControllerAdvice {

//    @ResponseBody // 以json形式返回所以使用 ResponseBody
    // 接收全部异常处理
    @ExceptionHandler(value = MethodArgumentNotValidException.class) // value = Exception.class 指定异常类型
    public R handleVaildException(MethodArgumentNotValidException e){
        log.error("数据校验出现问题:{},异常类型:{}",e.getMessage(),e.getClass());
        BindingResult bindingResult = e.getBindingResult();
//         收集每一个错误的 字段 及 错误提示消息 封装map返回
            Map<String,String> errorMap = new HashMap<>();
            // 获取所有检验结果
            bindingResult.getFieldErrors().forEach((fieldError)->{
                // 遍历每一个错误
                String defaultMessage = fieldError.getDefaultMessage(); // 获取到的校验消息错误提示
                String field = fieldError.getField(); // 发生错误的字段
                errorMap.put(field,defaultMessage);  // 放入到map中
            });
        return R.error(BizCodeEnum.VALID_EXCEPTION.getCode(),BizCodeEnum.VALID_EXCEPTION.getMsg()).put("data",errorMap);
    }

    // 公共 异常处理方法 可以处理所有异常
    @ExceptionHandler(value = Throwable.class) // value = Exception.class 指定异常类型
    public R handleVaildException(Throwable throwable) {
        log.error("错误：",throwable);
        return R.error(BizCodeEnum.UNKNOW_EXCEPTIO.getCode(),BizCodeEnum.UNKNOW_EXCEPTIO.getMsg());
    }

}
