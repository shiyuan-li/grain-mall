package com.atguigu.grainmall.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/8 14:01
 * @Version 1.0
 **/

@Data
public class SkuItemSaleAttrsVo {

    /**
     * 商品属性id
     */
    private Long attrId;

    /**
     *商品属性名字
     */
    private String attrName;

    /**
     *商品属性值
     */
    private List<AttrValueWithSkuIdVo> attrValues;

}
