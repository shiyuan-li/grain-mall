package com.atguigu.grainmall.product.feign;

import com.atguigu.common.to.SkuReductionTo;
import com.atguigu.common.to.SpuBoundTo;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Author shiyuan
 * @Date 2023/2/16 12:45
 * @Version 1.0
 * 保存spu积分信息-grainmall_sms->sms_spu_bounds   模块调用
 **/

@FeignClient("grainmall-coupon")
public interface CouponFeignService {

    /**
     * 保存spu积分信息-grainmall_sms->sms_spu_bounds   模块调用
     * @param spuBoundTo
     */
    @PostMapping("coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);

    /**
     * sku的优惠券、满减等信息-grainmall_sms->sms_sku_ladder、sms_sku_full_reduction、sms_member_price
     * @param skuReductionTo
     */
    @PostMapping("coupon/skufullreduction/saveInfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
