package com.atguigu.grainmall.product.dao;

import com.atguigu.grainmall.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性&属性分组关联
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {

    /**
     * 删除属性与分组的关联关系
     * @param collectVos
     * @return
     */
    void deleteBachRelation(@Param("collectVos") List<AttrAttrgroupRelationEntity> collectVos);
}
