package com.atguigu.grainmall.product.dao;

import com.atguigu.grainmall.product.entity.SkuSaleAttrValueEntity;
import com.atguigu.grainmall.product.vo.SkuItemSaleAttrsVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sku销售属性&值
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
@Mapper
public interface SkuSaleAttrValueDao extends BaseMapper<SkuSaleAttrValueEntity> {

    /**
     * 获取spu的销售属性组合
     *
     * @param spuId
     * @return
     */
    List<SkuItemSaleAttrsVo> getSaleAttrsBySpuId(@Param("spuId") Long spuId);

    /**
     * 获取商品属性值信息
     *
     * @param skuId
     * @return
     */
    List<String> getSkuSaleAttrValueAsStringList(@Param("skuId") Long skuId);
}
