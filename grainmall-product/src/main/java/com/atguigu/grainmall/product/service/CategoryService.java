package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.vo.Catelog2Vo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查出所有分类与子分类，以树形结构组装起来
     * @return
     */
    List<CategoryEntity> listWithTree();

    /**
     * 批量删除
     */
    void removeMenuByIds(List<Long> asList);

    /**
     * 查询完整三级菜单路径
     * @param catelogId
     * @return
     */
    Long[] findCategoryPath(Long catelogId);

    /**
     * 级联修改
     */
    void updateCascade(CategoryEntity category);

    /**
     * 查询一级分类
     * @return
     */
    List<CategoryEntity> getLevel1Categorys();

    /**
     * 将分类列表以json格式返回
     * @return
     */
    Map<String, List<Catelog2Vo>> getCatalogJson();
}

