package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:22
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 新增品牌与分类关联关系 - 保存
     */
    void saveDtail(CategoryBrandRelationEntity categoryBrandRelation);

    /**
     * 更新关联表中的品牌名
     * @param brandId
     * @param name
     */
    void updateBrand(Long brandId, String name);

    /**
     * 级联修改
     */
    void updateCategory(Long catId, String name);

    /**
     * 获取分类关联的品牌
     * @param catId
     * @return
     */
    List<BrandEntity> getBrandsByCatId(Long catId);

}

