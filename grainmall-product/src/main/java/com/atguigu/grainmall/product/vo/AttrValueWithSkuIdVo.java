package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/3/9 9:33
 * @Version 1.0
 **/
@Data
public class AttrValueWithSkuIdVo {

    private String attrValue;

    private String skuIds;

}
