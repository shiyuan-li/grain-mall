package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/2/11 18:56
 * @Version 1.0
 * 品牌vo
 **/
@Data
public class BrandVo {

    /**
     * "brandId": 0,
     *  "brandName": "string",
     */

    /**
     * 品牌id
     */
    private Long brandId;

    /**
     * 品牌名称
     */
    private String brandName;

}
