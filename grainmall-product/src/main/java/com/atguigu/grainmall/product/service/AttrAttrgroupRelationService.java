package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.vo.AttrGroupRelationVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.AttrAttrgroupRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 添加属性与分组关联关系 批量保存
     * @param vos
     * @return
     */
    void saveBatch(List<AttrGroupRelationVo> vos);
}

