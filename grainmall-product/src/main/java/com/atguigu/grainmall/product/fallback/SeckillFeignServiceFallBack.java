package com.atguigu.grainmall.product.fallback;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import com.atguigu.grainmall.product.feign.SeckillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-28 17:43:16、
 * 熔断降级  只要降级了 就会触发熔断机制 返回本地实现处理
 * 远程实现出错后调用本地实现返回指定处理
 * 可以在调用方设置降级 也可以在被调用方设置 但是一般不会在被调用方做降级操作 除非做牺牲一些远程服务时 在被调用方做降级返回降级数据
 */
@Slf4j
@Component
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckilInfo(Long skuId) {
        log.info("熔断方法调用-->getSkuSeckilInfo");
        return R.error(BizCodeEnum.TO_MANY_REQUEST.getCode(),BizCodeEnum.TO_MANY_REQUEST.getMsg());
    }
}
