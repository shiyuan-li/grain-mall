package com.atguigu.grainmall.product.vo;

import com.atguigu.grainmall.product.entity.SkuImagesEntity;
import com.atguigu.grainmall.product.entity.SkuInfoEntity;
import com.atguigu.grainmall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/7 12:32
 * @Version 1.0
 * 展示当前属性的详情vo
 **/
@Data
public class SkuItemVo {

    /**
     * sku基本信息
     */
    SkuInfoEntity info;

    /**
     * 有无货
     */
    boolean hasStock= true;

    /**
     * sku图片信息
     */
    List<SkuImagesEntity> images;

    /**
     *  获取spu的销售属性组合
     */
    List<SkuItemSaleAttrsVo> saleAttrs;

    /**
     * 获取spu的介绍
     */
    SpuInfoDescEntity desc;

    /**
     * 获取spu的规格参数信息
     */
    List<SpuItemAttrGroupVo> groupAttrs;

    /**
     * 商品秒杀优惠信息
     */
    SeckillSkuVo seckillSkuVo;

//    // 属性详情vo
//    @Data
//    public static class SkuItemSaleAttrsVo{
//        /**
//         * 商品属性id
//         */
//        private Long attrId;
//
//        /**
//         *商品属性名字
//         */
//        private String attrName;
//
//        /**
//         *商品属性值
//         */
//        private List<String> attrValues;
//    }

//    // 详情属性分组
//    @Data
//    public static class SpuItemAttrGroupVo{
//        /**
//         * 分组名
//         */
//        private String groupName;
//        /**
//         * 分组属性值
//         */
//        private List<SpuBaseAttrVo> attrs;
//
//    }

//    // 基本属性
//    @Data
//    public static class SpuBaseAttrVo{
//        /**
//         *商品属性名字
//         */
//        private String attrName;
//
//        /**
//         *商品属性值
//         */
//        private String attrValue;
//    }
}
