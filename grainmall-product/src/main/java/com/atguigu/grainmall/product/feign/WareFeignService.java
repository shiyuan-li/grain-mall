package com.atguigu.grainmall.product.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/2/21 12:59
 * @Version 1.0
 * 商品上架 - 查询是否有库存
 **/
@FeignClient("grainmall-ware")
public interface WareFeignService {

    /**
     * 查询是否有库存
     * @param skuIds
     * @return
     */
    @PostMapping("/ware/waresku/hasstock")
    R getSkusHasStock(@RequestBody List<Long> skuIds);

}
