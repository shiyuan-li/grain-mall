package com.atguigu.grainmall.product.entity;

import com.atguigu.common.vaild.AddGroup;
import com.atguigu.common.vaild.ListValue;
import com.atguigu.common.vaild.UpdateGroup;
import com.atguigu.common.vaild.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
//	@Null(message = "新增不能指定品牌id",groups = {AddGroup.class})
	@NotNull(message = "修改必须指定品牌id",groups = UpdateGroup.class) // groups 表示分组检验 该校验在那种情况下需要进行校验
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotNull  // 不可为空注解
//	@NotEmpty // 至少有一个非null字符 可能是一个空格 一种规则
	@NotBlank(message = "品牌名必须提交") // 可以自定义返回消息
	private String name;
	/**
	 * 品牌logo地址
	 */
	@NotEmpty(groups = {AddGroup.class})
	@URL(message = "品牌logo必须是一个合法的url地址",groups = {AddGroup.class, UpdateGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(groups = {AddGroup.class, UpdateStatusGroup.class})
	@ListValue(values = {0,1},groups = {AddGroup.class, UpdateStatusGroup.class}) // 自定义校验注解
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotEmpty(groups = {AddGroup.class})
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索字母必须是a-z或A-Z之间的一个",groups = {AddGroup.class, UpdateGroup.class})  // 自定义校验规则 可以使用正则表达式
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotNull(groups = {AddGroup.class})
	@Min(value = 0,message = "排序字段必须是一个从0开始的整数 并且大于等于0",groups = {AddGroup.class, UpdateGroup.class}) // 设置最小为多少
	private Integer sort;

}
