package com.atguigu.grainmall.product.service.impl;

import com.atguigu.common.constant.ProductConstant;
import com.atguigu.grainmall.product.dao.AttrAttrgroupRelationDao;
import com.atguigu.grainmall.product.dao.AttrGroupDao;
import com.atguigu.grainmall.product.dao.CategoryDao;
import com.atguigu.grainmall.product.entity.AttrAttrgroupRelationEntity;
import com.atguigu.grainmall.product.entity.AttrGroupEntity;
import com.atguigu.grainmall.product.entity.CategoryEntity;
import com.atguigu.grainmall.product.service.CategoryService;
import com.atguigu.grainmall.product.vo.AttrGroupRelationVo;
import com.atguigu.grainmall.product.vo.AttrResponseVo;
import com.atguigu.grainmall.product.vo.AttrVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.grainmall.product.dao.AttrDao;
import com.atguigu.grainmall.product.entity.AttrEntity;
import com.atguigu.grainmall.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    AttrAttrgroupRelationDao relationDao;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 保存
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttr(AttrVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        // 将attrVo中的数据放进AttrEntity中，诸葛赋值太麻烦 直接使用BeanUtils工具类直接进行复制
        BeanUtils.copyProperties(attr,attrEntity);
        // 保存基本数据
        this.save(attrEntity);
        // 判断类型为1的才去做保存  参数进行区分 是1 为基础属性 0 为销属性
        if (attr.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode() && !StringUtils.isEmpty(attr.getAttrGroupId())) {
            // 保存关联关系
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            relationEntity.setAttrId(attrEntity.getAttrId());
            relationDao.insert(relationEntity);
        }
    }


    /**
     * 获取分类的属性参数 进行分页查询  与 销售属性列表 以请求过俩的 Type 参数进行区分 是1 为销售属性 0 为分类属性 因为功能性相似 直接微调
     * 加入了eq(AttrEntity::getAttrType,"base".equalsIgnoreCase(type) ? 1 : 0)
     * IgnoreCase表示忽略大小写
     * @param param
     * @param catelogId
     * @param type
     */
    @Override
    public PageUtils queryBaseList(Map<String, Object> param, Long catelogId, String type) {
        LambdaQueryWrapper<AttrEntity> wrapper = new LambdaQueryWrapper<AttrEntity>()
                // 参数进行区分 是1 为基础属性 0 为销售属性 因为功能性相似 直接微调
                .eq(AttrEntity::getAttrType,"base".equalsIgnoreCase(type) ? ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode() : ProductConstant.AttrEnum.ATTR_TYPE_CALE.getCode());
        // 根据不同的情况判断是全部查询还是 模糊查询 还要对查询类型进行判断 看是那个查询 通过attrType 判断
        if (catelogId != 0){
            // 不等于0 那么就需要 模糊查询 拼接查询体哦阿健
            wrapper.eq(AttrEntity::getCatelogId,catelogId);
        }
        String key = (String) param.get("key");
        // 判断模糊查询是否 匹配某些字段
        if (!StringUtils.isEmpty(key)){
            wrapper.and((queryWrapper)->{
                queryWrapper.eq(AttrEntity::getAttrId,key).or().like(AttrEntity::getAttrName,key);
            });
        }
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(param),
                wrapper
        );
        PageUtils pageUtils = new PageUtils(page);
        // 获取page中的当前 记录（数据）
        List<AttrEntity> records = page.getRecords();
        // 使用查询好的分数据 进行处理
        List<AttrResponseVo> responseVoList = records.stream().map((attrEntity) -> {
            // 当前实体趋势两条数据 遍历映射每一个元素 最终返回 完整的 AttrResponseVo
            AttrResponseVo attrResponseVo = new AttrResponseVo();
            // 将当前attrEntity中的元素对拷过来
            BeanUtils.copyProperties(attrEntity, attrResponseVo);
            // 判断是否为 规格参数 是的话进行设置分组 不是的话不进行设置 因为 销售属性中 没有分组
            if ("base".equalsIgnoreCase(type)) {
                // 从中间表中先查出 分组id
                AttrAttrgroupRelationEntity attrId = relationDao.selectOne(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                        .eq(AttrAttrgroupRelationEntity::getAttrId, attrEntity.getAttrId()));
                // 做非空判断 容错
                if (!StringUtils.isEmpty(attrId) && !StringUtils.isEmpty(attrId.getAttrGroupId())) {
                    // 根据分组id查出 实体 获取到 分组名称
                    AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrId.getAttrGroupId());
                    // 设置缺少的 分组名称 进行查询复制进来 后统一返回
                    attrResponseVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }
            // 设置缺少的 分类名称
            CategoryEntity categoryEntity = categoryDao.selectById(attrEntity.getCatelogId());
            // 做非空判断 容错
            if (!StringUtils.isEmpty(categoryEntity)) {
                // 设置缺少的 分类名称 进行查询复制进来 后统一返回
                attrResponseVo.setCatelogName(categoryEntity.getName());
            }
            return attrResponseVo;
        }).collect(Collectors.toList());
        // 将最新的完整数据设置返回
        pageUtils.setList(responseVoList);

        return pageUtils;
    }

    /**
     * 获取属性信息详情
     */
    @Override
    public AttrResponseVo getAttrIofo(Long attrId) {
        AttrResponseVo responseVo = new AttrResponseVo();
        // 查询到全部详细信息
        AttrEntity attrEntity = this.getById(attrId);
        // 将查询到的信息先拷贝过来
        BeanUtils.copyProperties(attrEntity, responseVo);
        // 判断类型为1的才去做保存  参数进行区分 是1 为基础属性 0 为销售属性
        if (attrEntity.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()) {
            // responseVo 还需要查询到分组信息
            AttrAttrgroupRelationEntity attrAttrgroupRelation = relationDao.selectOne(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().eq(AttrAttrgroupRelationEntity::getAttrId, attrId));
            // 进行非空空判断 设置分组信息
            if (attrAttrgroupRelation != null) {
                responseVo.setAttrGroupId(attrAttrgroupRelation.getAttrGroupId());
                AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrAttrgroupRelation.getAttrGroupId());
                if (attrGroupEntity != null) {
                    responseVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }
        }
        // responseVo 还需要查询到完整分类路径
        Long catelogId = attrEntity.getCatelogId();
        Long[] categoryPath = categoryService.findCategoryPath(catelogId);
        responseVo.setCatelogPath(categoryPath);
        CategoryEntity categoryEntity = categoryDao.selectById(catelogId);
        if (categoryEntity != null){
            responseVo.setCatelogName(categoryEntity.getName());
        }

        return responseVo;
    }

    /**
     * 修改
     * @param attr
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAttr(AttrVo attr) {
        // 基础数据修改
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr,attrEntity);
        this.updateById(attrEntity);
        // 判断类型为1的才去做保存  参数进行区分 是1 为基础属性 0 为销售属性
        if (attrEntity.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()) {
            // 分组关联表修改
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrId(attr.getAttrId());
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            Integer count = relationDao.selectCount(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().eq(AttrAttrgroupRelationEntity::getAttrId, attr.getAttrId()));
            // 通过查询条数判断是新增操作还是修改操作
            if (count > 0) {
                // 修改
                relationDao.update(relationEntity, new LambdaUpdateWrapper<AttrAttrgroupRelationEntity>().eq(AttrAttrgroupRelationEntity::getAttrId, attr.getAttrId()));
            } else {
                // 新增插入
                relationDao.insert(relationEntity);
            }
        }
    }

    /**
     * 根据分组id获取关联的所有基本属性
     * @param attrgroupId
     * @return
     */
    @Override
    public List<AttrEntity> getRelationAttr(Long attrgroupId) {

        // 查询关联表信息
        List<AttrAttrgroupRelationEntity> relationEntities = relationDao.selectList(
                new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                        .eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrgroupId));

        // 收集属性id
        List<Long> attrIds = relationEntities.stream().map((attr) -> {
            return attr.getAttrId();
        }).collect(Collectors.toList());
        if (attrIds == null || attrIds.size() == 0){
            return null;
        }
        // 根据id查出所有的属性信息
        List<AttrEntity> attrEntitiesList = this.baseMapper.selectBatchIds(attrIds);
        return attrEntitiesList;
    }

    /**
     * 删除属性与分组的关联关系
     * @param vos
     * @return
     */
    @Override
    public void deleteRelation(AttrGroupRelationVo[] vos) {
        // 将vos 封装一下
        List<AttrAttrgroupRelationEntity> collectVos = Arrays.asList(vos).stream().map((item) -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            // 将 遍历出来的每一个vos拷贝到relationEntity中
            BeanUtils.copyProperties(item, relationEntity);
            // 返回当前的relationEntity
            return relationEntity;
        }).collect(Collectors.toList());
        // 只需要删除关联关系 保留属性 批量删除
        relationDao.deleteBachRelation(collectVos);
    }

    /**
     * 获取属性分组没有关联的其他属性
     * @param params
     * @param attrgroupId
     * @return
     */
    @Override
    public PageUtils getRelationNoAttr(Map<String, Object> params, Long attrgroupId) {
        /**
         * 当前分组只能关联自己所属的分类里面的所有属性
         */
        // 先查出当前分组的id
        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupId);
        // 拿到分类id
        Long catelogId = attrGroupEntity.getCatelogId();
        /**
         * 当前分组只能关联别的分组没有引用的属性
         */
        // 拿到 当前分类下的其他分组
        List<AttrGroupEntity> attrGroupEntities = attrGroupDao.selectList(new LambdaQueryWrapper<AttrGroupEntity>()
                .eq(AttrGroupEntity::getCatelogId, catelogId));
//                .ne(AttrGroupEntity::getAttrGroupId,attrgroupId));
        // 收集封装 attrGroupEntities 中的 分组id
        List<Long> groupList = attrGroupEntities.stream().map((item) -> {
            return item.getAttrGroupId();
        }).collect(Collectors.toList());
        // 拿到这些分组的关联的属性id
        List<AttrAttrgroupRelationEntity> groupId = relationDao.selectList(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                .in(AttrAttrgroupRelationEntity::getAttrGroupId, groupList));
        // 收集封装 groupList 中的 分组分组关联的属性id
        List<Long> attrIds = groupId.stream().map((item) -> {
            return item.getAttrId();
        }).collect(Collectors.toList());
        // 从当前分类的所有属性表中移除这些属性 得到没有关联的这些写属性 并且只能是基础属性 ATTR_TYPE_BASE = 1 的
        LambdaQueryWrapper<AttrEntity> Wrapper = new LambdaQueryWrapper<AttrEntity>()
                .eq(AttrEntity::getCatelogId, catelogId)
                .eq(AttrEntity::getAttrType,ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
                if(attrIds != null && attrIds.size() > 0){
                    Wrapper.notIn(AttrEntity::getAttrId, attrIds);
                }
        String key = (String) params.get("key");
        // 判断是否包含 模糊查询
        if (!StringUtils.isEmpty(key)){
            // 拼接模糊查询条件
            Wrapper.and((w)->{
                w.eq(AttrEntity::getAttrId,key).or().like(AttrEntity::getAttrName,key);
            });
        }
        // 用分页方法进行查询 返回数据
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), Wrapper);
        // 得到返回的 分页数据
        PageUtils pageUtils = new PageUtils(page);

        return pageUtils;
    }

    /**
     * 查询当前sku得所有可以被检索的规格属性
     * @param attrIds
     * @return
     */
    @Override
    public List<Long> selectSearchAttrIds(List<Long> attrIds) {

        return this.baseMapper.selectSearchAttrIds(attrIds);
    }

}
