package com.atguigu.grainmall.product.dao;

import com.atguigu.grainmall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:22
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}
