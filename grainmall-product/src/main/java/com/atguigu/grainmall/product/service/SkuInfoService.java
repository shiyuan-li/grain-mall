package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.vo.SkuItemVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.SkuInfoEntity;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * sku信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:22
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存sku的基本信息-pms_sku_info
     * @param skuInfoEntity
     */
    void saveSkuInfo(SkuInfoEntity skuInfoEntity);

    /**
     * 列表 sku检索
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 查出spuid对应的所有信息 品牌的名字
     * @param spuId
     * @return
     */
    List<SkuInfoEntity> getSkusBSpuId(Long spuId);

    /**
     * 展示当前水库的详情
     * @param skuId
     * @return
     */
    SkuItemVo item(Long skuId) throws ExecutionException, InterruptedException;
}

