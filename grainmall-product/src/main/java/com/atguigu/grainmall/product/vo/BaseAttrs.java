/** Copyright 2020 bejson.com */
package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2020-05-31 11:3:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 * 上架保存vo
 */

@Data
public class BaseAttrs {

  /**
   *商品属性id
   */
  private Long attrId;

  /**
   *商品属性值
   */
  private String attrValues;

  /**
   *显示排序
   */
  private int showDesc;

}
