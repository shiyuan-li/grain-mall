package com.atguigu.grainmall.product.app;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.atguigu.common.vaild.AddGroup;
import com.atguigu.common.vaild.UpdateStatusGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atguigu.grainmall.product.entity.BrandEntity;
import com.atguigu.grainmall.product.service.BrandService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;


/**
 * 品牌
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 21:40:29
 */
@Slf4j
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:brand:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    //@RequiresPermissions("product:brand:info")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    /**
     * 保存- 数据校验及返回格式 demo
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:brand:save") @Valid是标准 @Validated是springfremwork提供的自定义分组接口注解 指定校验分组为AddGroup.class
    public R save(@Validated({AddGroup.class}) @RequestBody BrandEntity brand){ // @Valid 为检验 开启注解 开启后启用检验功能 告诉mvc有需要校验字段  BindingResult result 封装了并可以获取到检验对象的自定义检验结果 不写表示将异常抛出
//		// 先判断数据是否校验通过
//        if(result.hasErrors()){
//            // 收集每一个错误的 字段 及 错误提示消息 封装map返回
//            Map<String,String> map = new HashMap<>();
//            // 获取所有检验结果
//            result.getFieldErrors().forEach((item)->{
//                // 遍历每一个错误
//                String defaultMessage = item.getDefaultMessage(); // 获取到的校验消息错误提示
//                String field = item.getField(); // 发生错误的字段
//                map.put(field,defaultMessage);  // 放入到map中
//            });
//            return R.error(400,"提交数据不合法").put("data",map);
//        }else {
//            // 通过才去执行相关操作
//            brandService.save(brand);
//        }
        brandService.save(brand);
        return R.ok();
    }
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:brand:update")
    public R update(@Validated({AddGroup.class}) @RequestBody BrandEntity brand){
		brandService.updateDatil(brand);

        return R.ok();
    }

    /**
     * 修改显示状态
     */
    @RequestMapping("/update/status")
    //@RequiresPermissions("product:brand:update")
    public R updateStatus(@Validated({UpdateStatusGroup.class}) @RequestBody BrandEntity brand){
        brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:brand:delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
