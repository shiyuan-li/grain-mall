package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.vo.SkuItemSaleAttrsVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.SkuSaleAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取spu的销售属性组合
     * @param spuId
     * @return
     */
    List<SkuItemSaleAttrsVo> getSaleAttrsBySpuId(Long spuId);

    /**
     * 获取商品属性值信息
     * @param skuId
     * @return
     */
    List<String> getSkuSaleAttrValueAsStringList(Long skuId);
}

