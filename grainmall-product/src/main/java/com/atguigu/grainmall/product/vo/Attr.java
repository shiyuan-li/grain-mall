/** Copyright 2020 bejson.com */
package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2020-05-31 11:3:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 * 上架保存vo
 */

@Data
public class Attr {

  /**
   * 商品属性id
   */
  private Long attrId;

  /**
   *商品属性名字
   */
  private String attrName;

  /**
   *商品属性值
   */
  private String attrValue;
}
