package com.atguigu.grainmall.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/8 13:59
 * @Version 1.0
 **/

@Data
public class SpuItemAttrGroupVo {

    /**
     * 分组名
     */
    private String groupName;
    /**
     * 分组属性值
     */
    private List<Attr> attrs;

}
