package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/2/9 16:31
 * @Version 1.0
 * 分组与属性 移除功能 接收数据
 **/
@Data
public class AttrGroupRelationVo {

    /**
     * 属性id
     */
    private Long attrId;
    /**
     * 属性分组id
     */
    private Long attrGroupId;

}
