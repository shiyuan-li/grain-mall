package com.atguigu.grainmall.product.dao;

import com.atguigu.grainmall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
