package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/2/2 16:01
 * @Version 1.0
 * 响应数据
 **/
@Data
public class AttrResponseVo extends AttrVo{

    /**
     * 所属分类名字
     */
    private String catelogName;
    /**
     * 所属分组名字
     */
    private String groupName;

    /**
     * 分类完整路径
     */
    private Long[] catelogPath;
}
