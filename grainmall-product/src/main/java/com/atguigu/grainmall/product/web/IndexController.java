package com.atguigu.grainmall.product.web;

import com.atguigu.grainmall.product.entity.CategoryEntity;
import com.atguigu.grainmall.product.service.CategoryService;
import com.atguigu.grainmall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author shiyuan
 * @Date 2023/2/23 14:32
 * @Version 1.0
 * 页面处理
 **/
@Controller
public class IndexController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    RedissonClient redisson;

    /**
     * 查询一级分类
     *
     * @param model
     * @return
     */
    @GetMapping({"/", "/index.html"})
    public String indexPage(Model model) {
        List<CategoryEntity> categoryEntityList = categoryService.getLevel1Categorys();
        model.addAttribute("categorys", categoryEntityList);
        return "index";
    }

    /**
     * 将分类列表以json格式返回
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/index/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        Map<String, List<Catelog2Vo>> catalogJson = categoryService.getCatalogJson();

        return catalogJson;
    }

    /**
     * 测试方法 -> Redisson
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/hello")
    public String hello() {
        // 获取但一把锁 只要是所得名字一样 就是统计一把锁 就可以锁住所有的服务
        RLock myLock = redisson.getLock("my-lock");
        // 加锁 竞争所失败之后 会持续自旋 等待锁资源 即阻塞式等待 看门狗 可以每搁 看门狗默认续期时间的 / 3 ，也就是执行定时任务 每 10妙自动续期
        //myLock.lock();
        // 指定过期时间
        myLock.lock(30, TimeUnit.SECONDS);
        try {
            System.out.println("加锁成功，执行业务....." + Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (Exception e) {

        } finally {
            // 无论执行是否成功顺利 都要将锁释放
            myLock.unlock();
            System.out.println("释放锁成功" + Thread.currentThread().getId());
        }
        return "hello";
    }

}
