package com.atguigu.grainmall.product.feign;

import com.atguigu.common.to.es.SkuEsModel;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/2/21 14:16
 * @Version 1.0
 * 检索服务
 **/
@FeignClient("grainmall-search")
public interface SearchFeignService {

    /**
     * 商品上架
     *
     * @param skuEsModels
     * @return
     */
    @PostMapping("/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels);

}
