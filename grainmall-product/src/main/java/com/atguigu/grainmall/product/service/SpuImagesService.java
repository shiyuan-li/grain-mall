package com.atguigu.grainmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.SpuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * spu图片
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:22
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存spu图片集-pms_spu_images
     * @param id
     * @param images
     */
    void saveImages(Long id, List<String> images);
}

