/** Copyright 2020 bejson.com */
package com.atguigu.grainmall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2020-05-31 11:3:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 * 上架保存vo
 */

@Data
public class Bounds {

  /**
   * 成长积分
   */
  private BigDecimal buyBounds;

  /**
   * 成长值
   */
  private BigDecimal growBounds;

}
