package com.atguigu.grainmall.product.service;

import com.atguigu.grainmall.product.vo.SpuSaveVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:22
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存
     */
    void saveSpuInfo(SpuSaveVo spuSaveVo);

    /**
     * 保存spu基本信息-pms_spu_info
     */
    void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity);

    /**
     * 列表  spu检索
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 商品上架
     * @param spuId
     * @return
     */
    void up(Long spuId);

    /**
     * 获取spu的信息
     * @param skuId
     * @return
     */
    SpuInfoEntity getSpuInfoBySkuId(Long skuId);
}

