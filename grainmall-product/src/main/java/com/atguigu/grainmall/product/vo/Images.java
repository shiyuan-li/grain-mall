/** Copyright 2020 bejson.com */
package com.atguigu.grainmall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2020-05-31 11:3:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 * 上架保存vo
 */

@Data
public class Images {

  /**
   *图片地址
   */
  private String imgUrl;

  /**
   *默认图片
   */
  private int defaultImg;

}
