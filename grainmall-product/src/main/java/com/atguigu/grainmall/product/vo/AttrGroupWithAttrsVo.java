package com.atguigu.grainmall.product.vo;

import com.atguigu.grainmall.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/2/12 16:17
 * @Version 1.0
 * 分组&关联属性vo
 **/
@Data
public class AttrGroupWithAttrsVo {

    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    /**
     * 属性
     */
    private List<AttrEntity> attrs;

}
