package com.atguigu.grainmall.product.dao;

import com.atguigu.grainmall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:11:23
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
