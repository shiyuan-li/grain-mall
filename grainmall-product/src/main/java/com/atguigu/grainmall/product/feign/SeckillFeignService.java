package com.atguigu.grainmall.product.feign;

import com.atguigu.common.utils.R;
import com.atguigu.grainmall.product.fallback.SeckillFeignServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-28 17:43:16
 */

@FeignClient(value = "grainmall-seckill",fallback = SeckillFeignServiceFallBack.class) // 做熔断降级相关参数 fallback远程实现出错后调用本地实现返回指定页面
public interface SeckillFeignService {

    /**
     * 根据skuId查询商品是否参加秒杀活动
     * @param skuId
     * @return
     */
    @GetMapping(value = "/sku/seckill/{skuId}")
    R getSkuSeckilInfo(@PathVariable("skuId") Long skuId);

}
