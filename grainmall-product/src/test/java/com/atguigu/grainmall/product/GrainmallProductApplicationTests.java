package com.atguigu.grainmall.product;


import com.atguigu.grainmall.product.dao.AttrGroupDao;
import com.atguigu.grainmall.product.entity.BrandEntity;
import com.atguigu.grainmall.product.service.BrandService;
import com.atguigu.grainmall.product.vo.SpuItemAttrGroupVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GrainmallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Test
    public void testRedisTemplate(){
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        // 保存数据 key value 形式
        ops.set("hello","world");

        // 查询
        String hello = ops.get("hello");
        System.out.println("保存的数据是："+ hello);
    }

//
//    @Resource
//    OSSClient ossClient;
//
//    /**
//     * 阿里云原生方式上传
//     */
//    @Test
//    public void  testUpload() {
//
//        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
//        String endpoint = "oss-cn-beijing.aliyuncs.com";
//        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
//        String accessKeyId = "LTAI5tJPG78wtYqzjEz1Lw7m";
//        String accessKeySecret = "tnchQq7ljVWFitMhXF4jb3foziap9Q";
//        // 填写Bucket名称，例如examplebucket。
//        String bucketName = "shaobingmall";
//        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
//        String objectName = "v2-3ec0b0c68ba898aa95e8f6afb4b37d62_r.jpg";
//        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
//        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
//        String filePath = "D:\\v2-3ec0b0c68ba898aa95e8f6afb4b37d62_r.jpg";
//
//        // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//
//        try {
//            InputStream inputStream = new FileInputStream(filePath);
//            // 创建PutObject请求。
//            ossClient.putObject(bucketName, objectName, inputStream);
//        } catch (OSSException oe) {
//            System.out.println("Caught an OSSException, which means your request made it to OSS, "
//                    + "but was rejected with an error response for some reason.");
//            System.out.println("Error Message:" + oe.getErrorMessage());
//            System.out.println("Error Code:" + oe.getErrorCode());
//            System.out.println("Request ID:" + oe.getRequestId());
//            System.out.println("Host ID:" + oe.getHostId());
//        } catch (ClientException ce) {
//            System.out.println("Caught an ClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with OSS, "
//                    + "such as not being able to access the network.");
//            System.out.println("Error Message:" + ce.getMessage());
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } finally {
//            if (ossClient != null) {
//                ossClient.shutdown();
//                System.out.println("文件上传完成.....!");
//            }
//        }
//    }
//
//    /**
//     * springboot整合阿里云方式上传
//     */
//    @Test
//    public void  testUpload_() throws FileNotFoundException {
//
//        // 上传文件流
//        InputStream inputStream = new FileInputStream("D:\\v2-46e15e27bc93d3bc0092701506920102_r.jpg");
//        // 传到那个空间，文件名叫啥
//        ossClient.putObject("shaobingmall","v2-46e15e27bc93d3bc0092701506920102_r.jpg",inputStream);
//        // 关闭文件流
//        ossClient.shutdown();
//        System.out.println("文件上传完成.....!");
//
//    }


    @Test
    public void contextLoads() {
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setName("小米");
//        brandService.save(brandEntity);
//        System.out.println("保存成功！");
//
//        brandService.queryPage()
        List<BrandEntity> brand_id_list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        brand_id_list.forEach((item)->{
            System.out.println("列表查询成功！"+ item);
        });
    }

    @Autowired
    AttrGroupDao attrGroupDao;

    @Test
    public void test(){
        List<SpuItemAttrGroupVo> attrGroupWithAttrsBySpuId = attrGroupDao.getAttrGroupWithAttrsBySpuId(13L,255L);
        System.out.println(attrGroupWithAttrsBySpuId.toString());
    }
}
