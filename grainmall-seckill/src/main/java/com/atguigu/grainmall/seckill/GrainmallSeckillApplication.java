package com.atguigu.grainmall.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

// sentnel 页面配置默认存储在内存中 重启服务后就会失效 必须使用持久化存储
@EnableRedisHttpSession
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class GrainmallSeckillApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallSeckillApplication.class, args);
    }

}
