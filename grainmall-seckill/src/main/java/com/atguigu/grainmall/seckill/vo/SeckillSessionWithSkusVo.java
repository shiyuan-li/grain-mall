package com.atguigu.grainmall.seckill.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-28 17:43:16
 */

@Data
public class SeckillSessionWithSkusVo {

    private Long id;
    /**
     * 场次名称
     */
    private String name;
    /**
     * 每日开始时间
     */
    private Date startTime;
    /**
     * 每日结束时间
     */
    private Date endTime;
    /**
     * 启用状态
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;

    private List<SeckillSkuVo> relationSkus;

}
