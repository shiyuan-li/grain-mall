package com.atguigu.grainmall.seckill.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @Author shiyuan
 * @Date 2023/3/14 16:51
 * @Version 1.0
 * session配置及序列化机制
 **/
@Configuration
public class GrainmallSessionConfig {

    /**
     * session配置Cookie
     * @return
     */
    @Bean
    public CookieSerializer cookieSerializer(){
        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
        // session作用域
        cookieSerializer.setDomainName("grainmall.com");
        // cookie名
        cookieSerializer.setCookieName("GRAINSESSION");

        return cookieSerializer;
    }

    /**
     * 序列化机制
     * @return
     */
    @Bean
    public RedisSerializer<Object> springSessionDefaultRedisSerializer() {
        return new GenericJackson2JsonRedisSerializer();
    }

}
