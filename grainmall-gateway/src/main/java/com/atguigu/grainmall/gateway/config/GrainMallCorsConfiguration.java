package com.atguigu.grainmall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * 跨域情求白名单 配置类
 * @Author shiyuan
 * @Date 2023/1/9 15:53
 * @Version 1.0
 **/
@Configuration
public class GrainMallCorsConfiguration {

    @Bean
    public CorsWebFilter corsWebFilter(){
        // 都是响应式包下的
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        /**
         * 跨域配置
         */
        // 配置那些请求头可以跨域访问
        corsConfiguration.addAllowedHeader("*");
        // 配置那些请求方式可以跨域访问
        corsConfiguration.addAllowedMethod("GET");
        corsConfiguration.addAllowedMethod("POST");
        corsConfiguration.addAllowedMethod("PUT");
        corsConfiguration.addAllowedMethod("DELETE");
        corsConfiguration.addAllowedMethod("OPTIONS");
        // 配置那些请求来源可以跨域访问
        corsConfiguration.addAllowedOrigin("*");
        // 是否允许携带Cookie 不配置则会丢失Cookie信息
        corsConfiguration.setAllowCredentials(true);
        // Cor 配置 访问路径
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);

        return new CorsWebFilter(urlBasedCorsConfigurationSource);
    }

}
