package com.atguigu.grainmall.search;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
class GrainmallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Test
    void test1() throws IOException {
//        Product product = new Product();
//        product.setSpuName("华为");
//        product.setId(10L);
        IndexRequest request = new IndexRequest("product").id("20")
                .source("spuName","华为","id",20L);
        try {
            IndexResponse response = client.index(request, RequestOptions.DEFAULT);
            System.out.println(request.toString());
            IndexResponse response2 = client.index(request, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
            }
        }
    }

}
