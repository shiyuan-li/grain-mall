package com.atguigu.grainmall.search.service;

import com.atguigu.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/2/21 13:37
 * @Version 1.0
 * 商品保存
 **/
public interface ProductSaveService {

    /**
     * 商品上架
     * @param skuEsModels
     * @return
     */
    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
