package com.atguigu.grainmall.search.constant;

/**
 * @Author shiyuan
 * @Date 2023/2/21 13:43
 * @Version 1.0
 * 常量
 **/
public class EsConstant {

    // sku数据在es中的索引
    public static final String PRODUCT_INDEX = "grainmall_product";

    // sku数据在es中的索引(分页使用)
    public static final Integer PRODUCT_PAGESIZE = 16;

}
