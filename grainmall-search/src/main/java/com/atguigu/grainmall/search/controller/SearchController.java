package com.atguigu.grainmall.search.controller;

import com.atguigu.grainmall.search.service.MallSearchService;
import com.atguigu.grainmall.search.vo.SearchParam;
import com.atguigu.grainmall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author shiyuan
 * @Date 2023/2/27 14:06
 * @Version 1.0
 * 检索页面跳转
 **/
@Controller
public class SearchController {


    @Autowired
    MallSearchService mallSearchService;

    /**
     * 数据检索
     * @param param
     * @return
     */
    @GetMapping("list.html")
    public String listPage(SearchParam param, Model model){
        // 根据查询参数，去es中检索商品
        SearchResult result = mallSearchService.search(param);
        // 将结果交给界面渲染
        model.addAttribute("result",result);

        return "list";
    }

}
