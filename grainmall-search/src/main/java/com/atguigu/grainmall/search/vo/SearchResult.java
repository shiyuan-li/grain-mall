package com.atguigu.grainmall.search.vo;

import com.atguigu.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/1 10:25
 * @Version 1.0
 * 检索数据相应信息
 **/
@Data
public class SearchResult {

    /**
     * 所有商品信息
     */
    private List<SkuEsModel> products;

    /**
     * 分页信息
     */
    private Integer pageNum;
    private Long total;
    private Integer totalPages;

    private List<BrandVo> brands;
    private List<AttrVo> attrs;
    private List<CatalogVo> catalogs;

    // ================以上是响应页面信息======================

    /**
     * 所有涉及到品牌的查询结果
     */
    @Data
    public static class BrandVo{
        private Long brandId;
        private String brandName;
        private String brandImg;
    }

    /**
     * 所有涉及到商品属性的查询结果
     */
    @Data
    public static class AttrVo{
        private Long attrId;
        private String attrName;
        private List<String> attrValue;
    }

    /**
     * 所有涉及到商品分类的查询结果
     */
    @Data
    public static class CatalogVo{
        private Long catalogId;
        private String catalogName;
    }
}
