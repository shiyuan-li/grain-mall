package com.atguigu.grainmall.search.service;

import com.atguigu.grainmall.search.vo.SearchParam;
import com.atguigu.grainmall.search.vo.SearchResult;

/**
 * @Author shiyuan
 * @Date 2023/3/1 9:46
 * @Version 1.0
 * 检索接口
 **/
public interface MallSearchService {

    /**
     * 数据检索
     * @param param
     * @return
     */
    SearchResult search(SearchParam param);
}
