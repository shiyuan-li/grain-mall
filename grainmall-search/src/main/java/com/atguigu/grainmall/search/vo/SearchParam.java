package com.atguigu.grainmall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/1 9:43
 * @Version 1.0
 * 封装所有页面检索参数
 **/
@Data
public class SearchParam {

    /**
     * 全文检索匹配关键字
     */
    private String keyword;

    /**
     * 三级分类id
     */
    private Long catalog3Id;

    /**
     * 排序条件
     */
    private String sort;

    /**
     * 是否只显示有货(1有库存，0没有，默认为1)
     */
    private Integer hasStock;

    /**
     * 价格区间查询
     */
    private String skuPrice;

    /**
     * 多个品牌查询
     */
    private List<Long> brandId;

    /**
     * 按照属性进行筛选
     */
    private List<String> attrs;

    /**
     * 页码
     */
    private Integer pageNum = 1;
}
