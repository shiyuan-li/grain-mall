package com.atguigu.grainmall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrainmallTestSsoClient2Application {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallTestSsoClient2Application.class, args);
    }

}
