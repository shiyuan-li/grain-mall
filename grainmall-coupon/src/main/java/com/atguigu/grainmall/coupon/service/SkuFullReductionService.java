package com.atguigu.grainmall.coupon.service;

import com.atguigu.common.to.SkuReductionTo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:21:31
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * sku的优惠券、满减等信息-grainmall_sms->sms_sku_ladder、sms_sku_full_reduction、sms_member_price
     * @param skuReductionTo
     * @return
     */
    void saveSkuReduction(SkuReductionTo skuReductionTo);
}

