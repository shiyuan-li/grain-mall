package com.atguigu.grainmall.coupon.dao;

import com.atguigu.grainmall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:21:32
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {

}
