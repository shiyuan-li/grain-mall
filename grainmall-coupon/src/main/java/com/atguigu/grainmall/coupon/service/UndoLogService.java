package com.atguigu.grainmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.coupon.entity.UndoLogEntity;

import java.util.Map;

/**
 *
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:21:32
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

