package com.atguigu.grainmall.coupon.dao;

import com.atguigu.grainmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:21:32
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {

}
