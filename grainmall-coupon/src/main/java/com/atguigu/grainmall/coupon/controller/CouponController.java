package com.atguigu.grainmall.coupon.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.atguigu.grainmall.coupon.entity.CouponEntity;
import com.atguigu.grainmall.coupon.service.CouponService;



/**
 * 优惠券信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 21:41:59
 */
@RefreshScope // 刷新配置中心配置
@RestController
@RequestMapping("coupon/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;
//  ####################################################################################
    /**
     * 供member服务调用 获取优惠券列表
     * @return
     */
    @GetMapping("/member/list")
    public R memberCoupons(){
        CouponEntity coupon = new CouponEntity();
        coupon.setCouponName("满100减1000");
        return R.ok().put("coupons",Arrays.asList(coupon));
    }
//  ####################################################################################

//  ------------------------------------------------------------------------------------
    /**
     * 测试 配置文件中读取变量值
     */
    //从配置文件中获取配置的变量
    @Value("${coupon.user.name}")
    private String name;
    @Value("${coupon.user.age}")
    private Integer age;
    @GetMapping("/test")
    public R testNacos_config(){

        return R.ok().put("name",name).put("age",age);
    }
//  ------------------------------------------------------------------------------------

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("coupon:coupon:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = couponService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("coupon:coupon:info")
    public R info(@PathVariable("id") Long id){
		CouponEntity coupon = couponService.getById(id);

        return R.ok().put("coupon", coupon);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("coupon:coupon:save")
    public R save(@RequestBody CouponEntity coupon){
		couponService.save(coupon);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("coupon:coupon:update")
    public R update(@RequestBody CouponEntity coupon){
		couponService.updateById(coupon);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("coupon:coupon:delete")
    public R delete(@RequestBody Long[] ids){
		couponService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
