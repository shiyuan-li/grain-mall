package com.atguigu.grainmall.coupon.service.impl;

import com.atguigu.grainmall.coupon.entity.SeckillSkuRelationEntity;
import com.atguigu.grainmall.coupon.service.SeckillSkuRelationService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.grainmall.coupon.dao.SeckillSessionDao;
import com.atguigu.grainmall.coupon.entity.SeckillSessionEntity;
import com.atguigu.grainmall.coupon.service.SeckillSessionService;


@Service("seckillSessionService")
public class SeckillSessionServiceImpl extends ServiceImpl<SeckillSessionDao, SeckillSessionEntity> implements SeckillSessionService {

    @Autowired
    SeckillSkuRelationService relationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SeckillSessionEntity> page = this.page(
                new Query<SeckillSessionEntity>().getPage(params),
                new QueryWrapper<SeckillSessionEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询最近三天需要参加秒杀商品的信息
     * @return
     */
    @Override
    public List<SeckillSessionEntity> getLates3DaySession() {
        List<SeckillSessionEntity> seckillSessionEntities = this.list(new LambdaQueryWrapper<SeckillSessionEntity>()
                .between(SeckillSessionEntity::getStartTime, startTime(), endTime()));
        // 再次查询出上架商品信息
        if (seckillSessionEntities != null && seckillSessionEntities.size() > 0){
            List<SeckillSessionEntity> list = seckillSessionEntities.stream().map(session -> {
                Long id = session.getId();
                List<SeckillSkuRelationEntity> relationEntities = relationService
                        .list(new LambdaQueryWrapper<SeckillSkuRelationEntity>()
                                .eq(SeckillSkuRelationEntity::getPromotionSessionId, id));
                session.setRelationSkus(relationEntities);

                return session;
            }).collect(Collectors.toList());

            return list;
        }
        return null;
    }

    /**
     * 起始时间
     * @return
     */
    private String startTime(){
        LocalDate now = LocalDate.now();
        LocalTime min = LocalTime.MIN;
        LocalDateTime start = LocalDateTime.of(now,min);
        String format = start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return format;
    }

    /**
     * 结束时间
     * @return
     */
    private String endTime(){
        LocalDate now = LocalDate.now();
        LocalTime max = LocalTime.MAX;
        LocalDate localDate = now.plusDays(2);
        LocalDateTime end = LocalDateTime.of(localDate, max);
        String format = end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return format;
    }

}
