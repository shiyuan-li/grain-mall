package com.atguigu.grainmall.coupon.dao;

import com.atguigu.grainmall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:21:31
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {

}
