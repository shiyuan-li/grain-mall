package com.atguigu.grainmall.ware.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/2/21 12:42
 * @Version 1.0
 * 查询是否有库存 vo
 **/
@Data
public class SkuHasStockVo {

   private Long skuId;

   private Boolean hasStock;
}
