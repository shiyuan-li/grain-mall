package com.atguigu.grainmall.ware.service;

import com.atguigu.grainmall.ware.vo.FareVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:23:53
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    /**
     * 模糊检索
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据用户收货地址计算获取运费
     *
     * @param addrId
     * @return
     */
    FareVo getFare(Long addrId);
}

