package com.atguigu.grainmall.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/2/17 15:32
 * @Version 1.0
 * 完成采购vo
 **/
@Data
public class PurchaseDoneVo {

    /**
     * 采购单id
     */
    @NotNull
    private Long id;

    /**
     * 采购项
     */
    private List<PurchaseItemDoneVo> items;

}
