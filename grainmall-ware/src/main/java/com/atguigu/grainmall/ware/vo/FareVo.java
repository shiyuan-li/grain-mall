package com.atguigu.grainmall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-19 13:17:10
 **/

@Data
public class FareVo {

    private MemberAddressVo address;

    private BigDecimal fare;

}


