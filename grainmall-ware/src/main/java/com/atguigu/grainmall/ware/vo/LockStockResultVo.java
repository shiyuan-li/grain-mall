package com.atguigu.grainmall.ware.vo;

import lombok.Data;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-21 13:44:14
 */

@Data
public class LockStockResultVo {

    private Long skuId;

    private Integer num;

    /** 是否锁定成功 **/
    private Boolean locked;

}
