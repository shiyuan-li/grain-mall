package com.atguigu.grainmall.ware.dao;

import com.atguigu.grainmall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:23:53
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
