package com.atguigu.grainmall.ware.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author shiyuan
 * @Date 2023/3/20 12:14
 * @Version 1.0
 **/
@FeignClient("grainmall-member")
public interface MemberFeignService {

    /**
     * 根据id获取会员信息  远程获取用户收获地地址
     */
    @RequestMapping("/member/memberreceiveaddress/info/{id}")
    R addrInfo(@PathVariable("id") Long id);

}
