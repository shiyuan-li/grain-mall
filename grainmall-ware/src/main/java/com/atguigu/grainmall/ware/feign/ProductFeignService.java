package com.atguigu.grainmall.ware.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author shiyuan
 * @Date 2023/2/17 17:05
 * @Version 1.0
 * 远程查询 sku 名字
 **/
@FeignClient("grainmall-gateway")
public interface ProductFeignService {

    /**      @FeignClient("grainmall-product")
     * 信息  直接去product模块找/product/skuinfo/info/{skuId}
     *      @FeignClient("grainmall-gateway")
     *      通过网关去找  /api/product/skuinfo/info/{skuId}
     */
    @RequestMapping("/api/product/skuinfo/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);

}
