package com.atguigu.grainmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.ware.entity.PurchaseDetailEntity;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:23:53
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    /**
     * 列表 + 查询采购需求
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 改变采购项的状态
     * @param id
     * @return
     */
    List<PurchaseDetailEntity> listDetailByPurchaseId(Long id);
}

