package com.atguigu.grainmall.ware;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableRabbit
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class GrainmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallWareApplication.class, args);
    }

}
