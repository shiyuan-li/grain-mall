package com.atguigu.grainmall.ware.service;

import com.atguigu.grainmall.ware.vo.MergeVo;
import com.atguigu.grainmall.ware.vo.PurchaseDoneVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.ware.entity.PurchaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:23:53
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询未领取的采购单
     * @param params
     * @return
     */
    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);

    /**
     * 合并采购需求
     * @param mergeVo
     * @return
     */
    void mergePurchase(MergeVo mergeVo);

    /**
     * 领取采购单
     * @param ids
     * @return
     */
    void received(List<Long> ids);

    /**
     * 列表 + 模糊查询
     * @param params
     * @return
     */
    PageUtils queryPagePurchase(Map<String, Object> params);

    /**
     * 完成采购单
     * @param doneVo
     * @return
     */
    void done(PurchaseDoneVo doneVo);
}

