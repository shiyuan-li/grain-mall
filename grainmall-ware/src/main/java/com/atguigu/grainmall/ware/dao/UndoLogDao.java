package com.atguigu.grainmall.ware.dao;

import com.atguigu.grainmall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:23:53
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
