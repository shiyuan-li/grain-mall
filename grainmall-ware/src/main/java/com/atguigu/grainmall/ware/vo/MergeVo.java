package com.atguigu.grainmall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/2/16 19:07
 * @Version 1.0
 * 采购单vo
 **/
@Data
public class MergeVo {

    /**
     * 整单id
     */
    private Long purchaseId;

    /**
     * 合并项集合
     */
    private List<Long> items;

}
