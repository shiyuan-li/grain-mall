package com.atguigu.grainmall.ware.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/2/17 15:33
 * @Version 1.0
 * 采购项vo
 **/
@Data
public class PurchaseItemDoneVo {

    /**
     * 采购项id
     */
    private Long itemId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 原因
     */
    private String reason;

}
