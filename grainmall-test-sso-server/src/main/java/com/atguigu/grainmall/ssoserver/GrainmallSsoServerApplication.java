package com.atguigu.grainmall.ssoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrainmallSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallSsoServerApplication.class, args);
    }

}
