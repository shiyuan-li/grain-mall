package com.atguigu.grainmall.cart.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/17 10:41
 * @Version 1.0
 * 购物车获取商品信息
 **/
@FeignClient("grainmall-product")
public interface ProductFeignService {

    /**
     * 根据id获取商品信息
     */
    @RequestMapping("/product/skuinfo/getSkuInfo/{skuId}")
    R getSkuInfo(@PathVariable("skuId") Long skuId);

    /**
     * 获取商品属性值信息
     * @param skuId
     * @return
     */
    @GetMapping("/product/skusaleattrvalue/stringlist/{skuId}")
    List<String> getSkuSaleAttrValue(@PathVariable("skuId") Long skuId);

    /**
     * 获取商品价格
     * @param skuId
     * @return
     */
    @GetMapping("/product/skuinfo/{skuId}/price")
    R getPrice(@PathVariable("skuId") Long skuId);
}
