package com.atguigu.grainmall.cart.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/16 8:47
 * @Version 1.0
 * 购物车对象整体
 *     需要进行计算的属性，都要重写get方法，保证每次获取属性都会重新计算
 **/
public class Cart {

    /**
     * 购物项
     */
    List<CartItem> items;

    /**
     * 商品数量
     */
    private Integer countNum;

    /**
     * 商品类型数量
     */
    private Integer countType;

    /**
     * 商品总价
     */
    private BigDecimal totalAmount;

    /**
     * 减免价格
     */
    private BigDecimal reducePrice = new BigDecimal("0.00");

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    /**
     * 自定义计算商品数量
     * @return
     */
    public Integer getCountNum() {
        int count = 0;
        if (items != null && items.size() > 0){
            for (CartItem item : items) {
                count += item.getCount();
            }
        }
        return count;
    }

    /**
     * 自定义计算商品类型
     * @return
     */
    public Integer getCountType() {
        int count = 0;
        if (items != null && items.size() > 0){
            for (CartItem item : items) {
                count += 1;
            }
        }
        return count;
    }

    /**
     * 自定义总价格
     * @return
     */
    public BigDecimal getTotalAmount() {
        // 计算购物项总价
        BigDecimal amount = new BigDecimal("0");
        if (items != null && items.size() > 0){
            for (CartItem item : items) {
                // 选中就叠加计算，否则不算
                if (item.getCheck()){
                    BigDecimal totalPrice = item.getTotalPrice();
                    amount = amount.add(totalPrice);
                }
            }
            // 计算减免价格 后的总价
            return amount.subtract(getReducePrice());
        }
        return amount;
    }


    public BigDecimal getReducePrice() {
        return reducePrice;
    }

    public void setReducePrice(BigDecimal reducePrice) {
        this.reducePrice = reducePrice;
    }
}
