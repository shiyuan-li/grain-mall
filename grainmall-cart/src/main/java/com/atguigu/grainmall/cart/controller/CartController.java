package com.atguigu.grainmall.cart.controller;

import com.atguigu.grainmall.cart.service.CartService;
import com.atguigu.grainmall.cart.vo.Cart;
import com.atguigu.grainmall.cart.vo.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @Author shiyuan
 * @Date 2023/3/16 9:33
 * @Version 1.0
 **/
@Controller
public class CartController {

    @Autowired
    CartService cartService;

    /**
     * 获取购物车中的购物项
     * @return
     */
    @ResponseBody
    @GetMapping("/currentUserCartItems")
    public List<CartItem> getCurrentUserCartItems(){

        return cartService.getCurrentUserCartItems();
    }

    /**
     * 去购物车页面的请求
     * 浏览器有一个cookie:user-key 标识用户的身份，一个月过期
     * 如果第一次使用jd的购物车功能，都会给一个临时的用户身份:
     * 浏览器以后保存，每次访问都会带上这个cookie；
     *
     * 登录：session有
     * 没登录：按照cookie里面带来user-key来做
     * 第一次，如果没有临时用户，自动创建一个临时用户
     *
     * @return
     */
    @GetMapping(value = "/cart.html")
    public String cartListPage(Model model) throws ExecutionException, InterruptedException {
        Cart cart = cartService.getCart();
        model.addAttribute("cart",cart);
        return "cartList";
    }

    /**
     * 添加商品到购物车
     * @return
     */
    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("skuId") Long skuId, @RequestParam("num") Integer num, RedirectAttributes redirectAttributes) throws ExecutionException, InterruptedException {

        CartItem cartItem = cartService.addToCart(skuId,num);
        // Model model
        //model.addAttribute("item",cartItem);
        // 重定向后携带数据
        redirectAttributes.addAttribute("skuId",skuId);
        return "redirect:http://cart.grainmall.com/addToCartSuccess.html";
    }

    /**
     * 添加商品到购物车后再次查询
     * 解决页面重复提交问题
     * @return
     */
    @GetMapping("/addToCartSuccess.html")
    public String addToCartSuccessPage(@RequestParam("skuId") Long skuId,Model model) {
        // 重定向到成功界面 解决页面重复提交问题
        CartItem cartItem = cartService.getCartItem(skuId);
        model.addAttribute("item",cartItem);

        return "success";
    }

    /**
     * 勾选购物车中的商品项
     * @param skuId
     * @param check
     * @return
     */
    @GetMapping("/checkItem")
    public String checkItem(@RequestParam("skuId") Long skuId,@RequestParam("check") Integer check){
        cartService.checkItem(skuId,check);

        return "redirect:http://cart.grainmall.com/cart.html";
    }

    /**
     * 增减数量按钮处理,修改购物车购物项数量
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,@RequestParam("num") Integer num){
        cartService.changeItemCount(skuId,num);

        return "redirect:http://cart.grainmall.com/cart.html";
    }

    /**
     * 删除购物项
     * @param skuId
     * @return
     */
    @GetMapping("/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId){
        cartService.deleteItem(skuId);

        return "redirect:http://cart.grainmall.com/cart.html";
    }

}
