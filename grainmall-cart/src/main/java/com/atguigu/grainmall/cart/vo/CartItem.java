package com.atguigu.grainmall.cart.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/16 8:48
 * @Version 1.0
 * 购物车中的每一个购物项
 *     需要进行计算的属性，都要重写get方法，保证每次获取属性都会重新计算
 **/

public class CartItem {

    /**
     * 商品id
     */
    private Long skuId;

    /**
     * 选中状态
     */
    private Boolean check = true;

    /**
     * 标题
     */
    private String title;

    /**
     * 商品图片
     */
    private String image;

    /**
     * 商品属性(包含套餐)
     */
    private List<String> skuAttr;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 小计
     */
    private BigDecimal totalPrice;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getSkuAttr() {
        return skuAttr;
    }

    public void setSkuAttr(List<String> skuAttr) {
        this.skuAttr = skuAttr;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 计算当前购物项总价
     * @return
     */
    public BigDecimal getTotalPrice() {
        BigDecimal multiply = this.price.multiply(new BigDecimal("" + this.count));

        return multiply;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

}
