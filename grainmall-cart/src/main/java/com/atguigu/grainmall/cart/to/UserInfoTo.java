package com.atguigu.grainmall.cart.to;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/3/16 9:52
 * @Version 1.0
 **/
@Data
public class UserInfoTo {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 临时键
     */
    private String userKey;

    /**
     * 标志位 是否有临时键
     */
    private Boolean tempUser = false;
}
