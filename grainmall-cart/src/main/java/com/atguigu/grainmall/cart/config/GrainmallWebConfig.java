package com.atguigu.grainmall.cart.config;

import com.atguigu.grainmall.cart.interceptor.CartInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author shiyuan
 * @Date 2023/3/16 10:15
 * @Version 1.0
 * 购物车页面拦截
 **/
@Configuration
public class GrainmallWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 拦截购物车所有请求
        registry.addInterceptor(new CartInterceptor()).addPathPatterns("/**");

    }
}
