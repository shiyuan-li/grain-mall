package com.atguigu.grainmall.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession // 开启redis-session功能
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class GrainmallAuthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrainmallAuthServerApplication.class, args);
	}

}
