package com.atguigu.grainmall.auth.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @Author shiyuan
 * @Date 2023/3/13 8:05
 * @Version 1.0
 * 用户注册vo
 **/

@Data
public class UserRegisterVo {

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名必须填写")
    @Length(min = 6,max = 18,message = "用户名必须为6~18为字符")
    private String userName;

    /**
     * 密码
     */
    @NotEmpty(message = "密码必须填写")
    @Length(min = 6,max = 18,message = "密码必须为6~18为字符")
    private String passWord;

    /**
     * 手机号
     */
    @NotEmpty(message = "电话号码必须填写")
    @Pattern(regexp = "^[1]([3-9])[0-9]{9}$",message = "手机号格式不正确")
    private String phone;

    /**
     * 验证码
     */
    @NotEmpty(message = "验证码必须填写")
    private String code;

}
