package com.atguigu.grainmall.auth.feign;

import com.atguigu.common.utils.R;
import com.atguigu.grainmall.auth.vo.SocialUser;
import com.atguigu.grainmall.auth.vo.UserLoginVo;
import com.atguigu.grainmall.auth.vo.UserRegisterVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Author shiyuan
 * @Date 2023/3/13 13:12
 * @Version 1.0
 * 注册
 **/
@FeignClient("grainmall-member")
public interface MemberFeignService {

    /**
     * 注册
     * @param vo
     * @return
     */
    @PostMapping("/member/member/register")
    R register(@RequestBody UserRegisterVo vo);

    /**
     * 登录
     * @param vo
     * @return
     */
    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    /**
     * 社交登录
     * @param socialUser
     * @return
     */
    @PostMapping("/member/member/oauth2/login")
    R oauth2Login(@RequestBody SocialUser socialUser)throws Exception;

}
