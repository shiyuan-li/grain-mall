package com.atguigu.grainmall.auth.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/3/13 13:55
 * @Version 1.0
 * 登录vo
 **/
@Data
public class UserLoginVo {

    /**
     * 用户名或手机号
     */
    private String loginacct;

    /**
     * 密码
     */
    private String password;

}
