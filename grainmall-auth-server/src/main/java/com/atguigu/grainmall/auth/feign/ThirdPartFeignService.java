package com.atguigu.grainmall.auth.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author shiyuan
 * @Date 2023/3/11 13:16
 * @Version 1.0
 * 短信服务远程调用
 **/
@FeignClient("grainmall-third-party")
public interface ThirdPartFeignService {

    /**
     * 发送验证码
     * @param phone
     * @param code
     * @return
     */
    @GetMapping(value = "/sms/sendCode")
    R sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code);

}
