package com.atguigu.grainmall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.grainmall.member.dao.MemberLevelDao;
import com.atguigu.grainmall.member.entity.MemberLevelEntity;
import com.atguigu.grainmall.member.exception.PhoneException;
import com.atguigu.grainmall.member.exception.UsernameException;
import com.atguigu.grainmall.member.vo.MemberLoginVo;
import com.atguigu.grainmall.member.vo.MemberRegisterVo;
import com.atguigu.grainmall.member.vo.SocialUser;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.grainmall.member.dao.MemberDao;
import com.atguigu.grainmall.member.entity.MemberEntity;
import com.atguigu.grainmall.member.service.MemberService;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 注册
     * @param vo
     */
    @Override
    public void register(MemberRegisterVo vo) {
        MemberEntity entity = new MemberEntity();
        MemberDao memberDao = this.baseMapper;
        // 设置默认会员等级
        MemberLevelEntity levelEntity = memberDao.getDefaultLevel();
        entity.setLevelId(levelEntity.getId());
        // 检查手机号与用户名的唯一性 为向上层调用方可以感知到是否有错误，使用异常机制向上层抛出异常
        checkPhoneUnique(vo.getPhone());
        checkUserNameUnique(vo.getUserName());
        // 设置信息
        entity.setMobile(vo.getPhone());
        entity.setUsername(vo.getUserName());
        entity.setNickname(vo.getUserName());
        /**
         *  密码加密存储 使用MD5盐值加密 单纯使用MD5是不安全的还会损失数据 必须配合盐值使用才可以达到不可逆的效果
         *  默认加盐规则是以：$1$ + 8位字符  ($1$88888888$需要加密的值)
         *  而且使用盐值加密 相同值的情况下只要盐不变 那么多次加密的结果也一样
         *  因为是不可逆的 所以验证密码时不能直接解密 而是将参数密码再次加密 对比加密之后的串是否相同
         *  使用时用官方密码加密器BCryptPasswordEncoder 同一值加密后的密文都不一样
         *  但是不影响验证 因为在BCryptPasswordEncoder定义好了每一次加密的盐值 每一次加密的颜值都会不同
         */
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(vo.getPassWord());
        entity.setPassword(encode);
        // 保存注册信息
        memberDao.insert(entity);
    }

    /**
     * 检验手机号唯一性
     * @param phone
     * @return
     */
    @Override
    public void checkPhoneUnique(String phone) throws PhoneException{
        MemberDao memberDao = this.baseMapper;
        Integer mobile = memberDao.selectCount(new LambdaQueryWrapper<MemberEntity>().eq(MemberEntity::getMobile, phone));
        if (mobile > 0){
            throw new PhoneException();
        }
    }

    /**
     * 检验用户名唯一性
     * @param userName
     * @return
     */
    @Override
    public void checkUserNameUnique(String userName) throws UsernameException{
        MemberDao memberDao = this.baseMapper;
        Integer Username = memberDao.selectCount(new LambdaQueryWrapper<MemberEntity>().eq(MemberEntity::getUsername, userName));
        if (Username > 0){
            throw new UsernameException();
        }
    }

    /**
     * 登录
     * @param vo
     * @return
     */
    @Override
    public MemberEntity login(MemberLoginVo vo) {
        String loginacct = vo.getLoginacct();
        String password = vo.getPassword();
        // 查询数据库
        MemberDao memberDao = baseMapper;
        MemberEntity memberEntity = memberDao.selectOne(
                new LambdaQueryWrapper<MemberEntity>()
                        .eq(MemberEntity::getUsername, loginacct)
                        .or()
                        .eq(MemberEntity::getMobile, loginacct));
        if (memberEntity == null){
            // 登陆失败
            return null;
        }else {
            // 获取到数据库路中的密码
            String passwordDb = memberEntity.getPassword();
            // 密码比对
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            boolean matchesPassword = passwordEncoder.matches(password, passwordDb);
            if (matchesPassword){
                return memberEntity;
            }else {
                return null;
            }

        }

    }

    /**
     * 社交登录
     * @param socialUser
     * @return
     */
    @Override
    public MemberEntity oauth2Login(SocialUser socialUser) throws Exception {
        // 需要有登陆与注册合并逻辑
        String uid = socialUser.getUid();
        // 判断当前社交用户是否在本系统登陆过
        MemberDao memberDao = baseMapper;
        MemberEntity member = memberDao.selectOne(new LambdaQueryWrapper<MemberEntity>().eq(MemberEntity::getSocialUid, uid));
        if (member != null){
            // 用户已经注册过了
            MemberEntity update = new MemberEntity();
            update.setId(member.getId());
            update.setAccessToken(socialUser.getAccess_token());
            update.setExpiresIn(socialUser.getExpires_in());

            memberDao.updateById(update);

            member.setAccessToken(socialUser.getAccess_token());
            member.setExpiresIn(socialUser.getExpires_in());
            return member;
        }else {
            // 没有查到社交登录对应信息 那就是第一次登陆 直接进行自动注册
            MemberEntity register = new MemberEntity();
            try {
                //查询当前社交用户的社交账号基础信息(昵称，性别等)
                Map<String, String> query = new HashMap<>();
                query.put("access_token", socialUser.getAccess_token());
                query.put("uid", socialUser.getUid());
                HttpResponse response = HttpUtils.doGet("https://api.weibo.com", "/2/users/show.json", "get", new HashMap<String, String>(), query);
                // 判断是否获取成功
                if (response.getStatusLine().getStatusCode() == 200) {
                    // 成功直接查询相关信息
                    String json = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = JSON.parseObject(json);
                    // 设置昵称，性别等信息
                    String name = jsonObject.getString("name");
                    String gender = jsonObject.getString("gender");
                    // 设置数据模型映射
                    register.setNickname(name);
                    register.setGender("m".equals(gender) ? 1 : 0);
                }
            }catch (Exception e){}
            // 保存注册及账号信息
            register.setSocialUid(socialUser.getUid());
            register.setAccessToken(socialUser.getAccess_token());
            register.setExpiresIn(socialUser.getExpires_in());
            memberDao.insert(register);

            return register;
        }
    }

}
