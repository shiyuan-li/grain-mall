package com.atguigu.grainmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.member.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:22:49
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

