package com.atguigu.grainmall.member.service;

import com.atguigu.grainmall.member.exception.PhoneException;
import com.atguigu.grainmall.member.exception.UsernameException;
import com.atguigu.grainmall.member.vo.MemberLoginVo;
import com.atguigu.grainmall.member.vo.MemberRegisterVo;
import com.atguigu.grainmall.member.vo.SocialUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.grainmall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:22:48
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 注册
     * @param vo
     */
    void register(MemberRegisterVo vo);

    /**
     * 检验手机号唯一性
     * @param phone
     * @return
     */
    void checkPhoneUnique(String phone) throws PhoneException;

    /**
     * 检验用户名唯一性
     * @param userName
     * @return
     */
    void checkUserNameUnique(String userName) throws UsernameException;

    /**
     * 登录
     * @param vo
     * @return
     */
    MemberEntity login(MemberLoginVo vo);

    /**
     * 社交登录
     * @param socialUser
     * @return
     */
    MemberEntity oauth2Login(SocialUser socialUser) throws Exception;
}

