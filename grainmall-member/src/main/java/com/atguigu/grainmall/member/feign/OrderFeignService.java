package com.atguigu.grainmall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @Author shiyuan
 * @Date 2023/3/28 7:58
 * @Version 1.0
 **/
@FeignClient("grainmall-order")
public interface OrderFeignService {

    /**
     * 分页获取当前登录用户的订单列表及订单项详情数据
     * @param params
     * @return
     */
    @PostMapping("/order/order/listWithItem")
    //@RequiresPermissions("order:order:list")
    R listWithItem(@RequestBody Map<String, Object> params);

}
