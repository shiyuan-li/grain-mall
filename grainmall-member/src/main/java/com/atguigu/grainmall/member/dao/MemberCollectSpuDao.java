package com.atguigu.grainmall.member.dao;

import com.atguigu.grainmall.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:22:48
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
