package com.atguigu.grainmall.member.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.grainmall.member.exception.PhoneException;
import com.atguigu.grainmall.member.exception.UsernameException;
import com.atguigu.grainmall.member.feign.CouponFeignService;
import com.atguigu.grainmall.member.vo.MemberLoginVo;
import com.atguigu.grainmall.member.vo.MemberRegisterVo;
import com.atguigu.grainmall.member.vo.SocialUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.atguigu.grainmall.member.entity.MemberEntity;
import com.atguigu.grainmall.member.service.MemberService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 会员
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 21:43:16
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    /**
     * 注册
     * @param vo
     * @return
     */
    @PostMapping("/register")
    public R register(@RequestBody MemberRegisterVo vo){

        try {
            memberService.register(vo);
        }catch (PhoneException phoneException){
            return R.error(BizCodeEnum.USER_EXIST_EXCEPTION.getCode(),BizCodeEnum.USER_EXIST_EXCEPTION.getMsg());
        }catch (UsernameException usernameException){
            return R.error(BizCodeEnum.PHONE_EXIST_EXCEPTION.getCode(),BizCodeEnum.PHONE_EXIST_EXCEPTION.getMsg());
        }
        return R.ok();
    }

    /**
     * 登录
     * @param vo
     * @return
     */
    @PostMapping("/login")
    public R login(@RequestBody MemberLoginVo vo){
        MemberEntity entity = memberService.login(vo);
        if (entity != null){
            return R.ok().setData(entity);
        }else {
            return R.error(BizCodeEnum.LOGINACCT_PASSWORD_INVATLD_EXCEPTION.getCode(),BizCodeEnum.LOGINACCT_PASSWORD_INVATLD_EXCEPTION.getMsg());
        }
    }

    /**
     * 社交登录
     * @param socialUser
     * @return
     */
    @PostMapping("/oauth2/login")
    public R oauth2Login(@RequestBody SocialUser socialUser) throws Exception {
        MemberEntity entity = memberService.oauth2Login(socialUser);
        if (entity != null){
            return R.ok().setData(entity);
        }else {
            return R.error(BizCodeEnum.LOGINACCT_PASSWORD_INVATLD_EXCEPTION.getCode(),BizCodeEnum.LOGINACCT_PASSWORD_INVATLD_EXCEPTION.getMsg());
        }
    }

//  ####################################################################################
    @Autowired
    private CouponFeignService couponFeignService;
    /**
     * 远程服务调用 Test 获取优惠券列表方法
     * @return
     */
    @GetMapping("/coupons")
    public R testMember_coupon(){
        MemberEntity member = new MemberEntity();
        member.setNickname("二狗");
        R memberCoupons = couponFeignService.memberCoupons();
        return R.ok().put("member",member).put("coupon",memberCoupons.get("coupons"));
    }
//  ####################################################################################
    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 根据id获取会员信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
