package com.atguigu.grainmall.member.config;

import com.atguigu.grainmall.member.interceptor.LoginUserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author shiyuan
 * @Date 2023/3/19 11:48
 * @Version 1.0
 * 拦截配置
 **/
@Configuration
public class MemberWebConfiguration implements WebMvcConfigurer {

    @Autowired
    LoginUserInterceptor loginUserInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截所有请求
        registry.addInterceptor(loginUserInterceptor).addPathPatterns("/**");
    }
}
