package com.atguigu.grainmall.member.dao;

import com.atguigu.grainmall.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:22:49
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
