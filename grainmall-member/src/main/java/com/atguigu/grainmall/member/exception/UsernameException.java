package com.atguigu.grainmall.member.exception;

/**
 * @Author shiyuan
 * @Date 2023/3/10 16:03
 * @Version 1.0
 * 登录与注册异常处理
 **/
public class UsernameException extends RuntimeException {


    public UsernameException() {
        super("用户名已存在");
    }
}
