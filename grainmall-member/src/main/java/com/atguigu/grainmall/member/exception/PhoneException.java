package com.atguigu.grainmall.member.exception;

/**
 * @Author shiyuan
 * @Date 2023/3/10 16:03
 * @Version 1.0
 * 登录与注册异常处理
 **/
public class PhoneException extends RuntimeException {

    public PhoneException() {
        super("手机号已存在");
    }
}
