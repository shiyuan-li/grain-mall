package com.atguigu.grainmall.member.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/3/14 9:57
 * @Version 1.0
 * 微博登录vo
 **/
@Data
public class SocialUser {

    private String access_token;

    private String remind_in;

    private String expires_in;

    private String uid;

    private String isRealName;

}
