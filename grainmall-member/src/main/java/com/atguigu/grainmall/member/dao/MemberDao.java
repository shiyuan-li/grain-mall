package com.atguigu.grainmall.member.dao;

import com.atguigu.grainmall.member.entity.MemberEntity;
import com.atguigu.grainmall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 *
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:22:48
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {

    /**
     * 获取默认会员等级
     * @return
     */
    MemberLevelEntity getDefaultLevel();
}
