package com.atguigu.grainmall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 供远程调用 接口
 */
@FeignClient("grainmall-coupon")//被调用方法的所在服务模块
public interface CouponFeignService {

//  ####################################################################################
    // 完整请求签名 地址 服务调用coupon下的优惠券列表
    @RequestMapping("/coupon/coupon/member/list")
    R memberCoupons();
//  ####################################################################################
}
