package com.atguigu.grainmall.member.vo;

import lombok.Data;


/**
 * @Author shiyuan
 * @Date 2023/3/13 11:57
 * @Version 1.0
 * 注册vo
 **/
@Data
public class MemberRegisterVo {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String passWord;

    /**
     * 手机号
     */
    private String phone;


}
