package com.atguigu.grainmall.member.vo;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/3/13 14:06
 * @Version 1.0
 * 登录vo
 **/
@Data
public class MemberLoginVo {

    /**
     * 用户名或手机号
     */
    private String loginacct;

    /**
     * 密码
     */
    private String password;

}
