package com.atguigu.grainmall.order;

import com.atguigu.grainmall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * @Author shiyuan
 * @Date 2023/3/18 10:44
 * @Version 1.0
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class GrainmallOrderApplicationTest {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 发送消息
     */
    @Test
    public void sendMessageTest(){
        OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
        orderReturnReasonEntity.setId(1L);
        orderReturnReasonEntity.setName("小黑");
        orderReturnReasonEntity.setCreateTime(new Date());
        String message = "HelloWorld!";
        // 发送消息 如果发送的是一个对象 需要使用序列化写出去 对象必须实现Serializable
        rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",orderReturnReasonEntity);
        // 序列化成json
        log.info("消息发送完成:{}",orderReturnReasonEntity);
    }

    /**
     * 收取消息
     */
//    @RabbitListener(queues = {"hello-java-Queue"}) // 监听 必须@EnableRabbit开启后才可以使用 在需要的方法上标注而且在容器中才能生效
//    @Test
//    public void recieveMessageTest(Message message,OrderReturnReasonEntity count){
//        // 获取消息体
//        byte[] body = message.getBody();
//        // 消息头
//        MessageProperties messageProperties = message.getMessageProperties();
//        log.info("接收到消息:{}",message);
////        System.out.println("接收到消息内容:"+message+"-->类型为:"+message.getClass());
//        System.out.println("接收到消息内容:"+count+"-->类型为:"+message);
//    }

    /**
     * 如何创建Exchange(交换机)、Queue(队列)、Binding(绑定)、
     *      使用AmqpAdmin进行创建
     * 如何收发消息
     */
    @Test
    public void createExchange(){
        // 创建(声明)一个Exchange(hello.java.exchange)
        // 单点交换机directExchange
        DirectExchange directExchange = new DirectExchange("hello-java-exchange",true,false);
        amqpAdmin.declareExchange(directExchange);
        log.info("Exchange[[{}]]创建成功","hello-java-exchange");
    }

    /**
     * 创建队列
     */
    @Test
    public void createQueue(){
        Queue queue = new Queue("hello-java-Queue",true,false,false);
        amqpAdmin.declareQueue(queue);
        log.info("Queue[[{}]]创建成功","hello-java-Queue");
    }

    /**
     * 创建绑定关系
     */
    @Test
    public void createBinding(){
        Binding binding = new Binding("hello-java-Queue", Binding.DestinationType.QUEUE,"hello-java-exchange","hello.java",null);
        amqpAdmin.declareBinding(binding);
        log.info("Binding[[{}]]创建成功","hello-java-binding");
    }
}
