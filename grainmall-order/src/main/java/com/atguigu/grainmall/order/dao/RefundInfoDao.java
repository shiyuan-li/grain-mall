package com.atguigu.grainmall.order.dao;

import com.atguigu.grainmall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2022-12-31 17:17:09
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
