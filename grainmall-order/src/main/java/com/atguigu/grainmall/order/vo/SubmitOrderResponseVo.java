package com.atguigu.grainmall.order.vo;

import com.atguigu.grainmall.order.entity.OrderEntity;
import lombok.Data;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-19 13:17:10
 **/

@Data
public class SubmitOrderResponseVo {

    private OrderEntity order;

    /** 错误状态码 0 为成功**/
    private Integer code;


}
