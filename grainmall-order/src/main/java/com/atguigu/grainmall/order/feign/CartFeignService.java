package com.atguigu.grainmall.order.feign;

import com.atguigu.grainmall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/19 14:14
 * @Version 1.0
 **/
@FeignClient("grainmall-cart")
public interface CartFeignService {

    /**
     * 获取购物车中的购物项
     * @return
     */
    @GetMapping("/currentUserCartItems")
    List<OrderItemVo> getCurrentUserCartItems();

}
