package com.atguigu.grainmall.order.vo;

import lombok.Data;

/**
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-19 13:17:10
 */
@Data
public class PayVo {

    private String out_trade_no; // 商户订单号 必填
    private String subject; // 订单名称 必填
    private String total_amount;  // 付款金额 必填
    private String body; // 商品描述 可空
}
