package com.atguigu.grainmall.order.constant;

/**
 * @Author shiyuan
 * @Date 2023/3/23 13:27
 * @Version 1.0
 **/
public class PayConstant {

    public static final Integer ALIPAY = 1;

    public static final Integer WXPAY = 2;

}
