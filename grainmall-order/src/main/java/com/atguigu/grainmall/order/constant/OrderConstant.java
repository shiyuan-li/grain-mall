package com.atguigu.grainmall.order.constant;

/**
 * @Author shiyuan
 * @Date 2023/3/20 13:57
 * @Version 1.0
 **/
public class OrderConstant {

    /**
     * 防重令牌前缀
     */
    public static final String USER_ORDER_TOKEN_PREFIX = "order:token:";

}
