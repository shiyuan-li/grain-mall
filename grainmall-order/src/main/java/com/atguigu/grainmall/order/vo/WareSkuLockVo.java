package com.atguigu.grainmall.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @Description: 锁定库存的vo
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-19 13:17:10
 **/

@Data
public class WareSkuLockVo {

    /** 订单号 **/
    private String orderSn;

    /** 需要锁住的所有库存信息 **/
    private List<OrderItemVo> locks;



}
