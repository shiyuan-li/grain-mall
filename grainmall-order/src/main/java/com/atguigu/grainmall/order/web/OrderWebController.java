package com.atguigu.grainmall.order.web;

import com.atguigu.common.exception.NoStockException;
import com.atguigu.grainmall.order.service.OrderService;
import com.atguigu.grainmall.order.vo.OrderConfirmVo;
import com.atguigu.grainmall.order.vo.OrderSubmitVo;
import com.atguigu.grainmall.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;

/**
 * @Author shiyuan
 * @Date 2023/3/19 11:43
 * @Version 1.0
 * 订单处理
 **/
@Controller
public class OrderWebController {

    @Autowired
    OrderService orderService;

    /**
     * 获取订单确认页的数据
     * @param model
     * @return
     */
    @GetMapping("/toTrade")
    public String toTrade(Model model, HttpServletRequest request) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("OrderConfirmOrder",confirmVo);
        // 展示订单确认页的数据
        return "confirm";
    }

    /**
     * 下单处理
     * @param submitVo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo submitVo, Model model, RedirectAttributes redirectAttributes){
        try {
            SubmitOrderResponseVo responseVo = orderService.submitOrder(submitVo);
            //下单成功来到支付选择页
            if (responseVo.getCode() == 0) {
                //成功
                model.addAttribute("submitOrderResp",responseVo);
                return "pay";
            } else {
                //下单失败回到订单确认页重新确定订单信息
                String msg = "下单失败:";
                switch (responseVo.getCode()) {
                    case 1: msg += "令牌订单信息过期，请刷新再次提交"; break;
                    case 2: msg += "订单商品价格发生变化，请确认后再次提交"; break;
                    case 3: msg += "库存锁定失败，商品库存不足"; break;
                }
                redirectAttributes.addFlashAttribute("msg",msg);
                return "redirect:http://order.grainmall.com/toTrade";
            }
        } catch (Exception e) {
            if (e instanceof NoStockException) {
                String message = ((NoStockException)e).getMessage();
                redirectAttributes.addFlashAttribute("msg",message);
            }
            return "redirect:http://order.grainmall.com/toTrade";
        }
    }

}
