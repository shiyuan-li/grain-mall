package com.atguigu.grainmall.order.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author shiyuan
 * @Date 2023/3/21 10:23
 * @Version 1.0
 **/
@FeignClient("grainmall-product")
public interface ProductFeignService {

    /**
     * 获取spu的信息
     * @param skuId
     * @return
     */
    @GetMapping("/product/spuinfo/skuId/{id}")
    R getSpuInfoBySkuId(@PathVariable("id") Long skuId);

}
