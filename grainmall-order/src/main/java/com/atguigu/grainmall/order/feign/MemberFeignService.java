package com.atguigu.grainmall.order.feign;

import com.atguigu.grainmall.order.vo.MemberAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @Author shiyuan
 * @Date 2023/3/19 13:32
 * @Version 1.0
 **/
@FeignClient("grainmall-member")
public interface MemberFeignService {

    /**
     * 获取会员收货信息列表
     * @param memberId
     * @return
     */
    @GetMapping("/member/memberreceiveaddress/{memberId}/addresses")
    List<MemberAddressVo> getAddress(@PathVariable("memberId") Long memberId);

}
