package com.atguigu.grainmall.order.vo;

import lombok.Data;

/**
 * @Description: 库存vo
 * @author shiyuan
 * @email aq30710@163.com
 * @date 2023-03-19 13:17:10
 **/

@Data
public class SkuStockVo {


    private Long skuId;

    private Boolean hasStock;

}
