package com.atguigu.grainmall.order.web;

import com.alipay.api.AlipayApiException;
import com.atguigu.grainmall.order.config.AlipayTemplate;
import com.atguigu.grainmall.order.service.OrderService;
import com.atguigu.grainmall.order.vo.PayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @Author shiyuan
 * @Date 2023/3/27 13:54
 * @Version 1.0
 **/
@Controller
public class PayWebController {

    @Autowired
    AlipayTemplate alipayTemplate;

    @Autowired
    OrderService orderService;

    /**
     * 用户下单:支付宝支付
     * 1、让支付页让浏览器展示
     * 2、支付成功以后，跳转到用户的订单列表页
     * @param orderSn
     * @return
     * @throws AlipayApiException
     */

    @ResponseBody
    @GetMapping(value = "/aliPayOrder",produces = "text/html") // produces = "text/html" 不屑默认返回的是json数据即(application/json)效果
    public String aliPayOrder(@RequestParam("orderSn") String orderSn) throws AlipayApiException {

        PayVo payVo = orderService.getOrderPay(orderSn);
        String pay = alipayTemplate.pay(payVo);
        // 支付宝返回的是一个页面，将此页面直接返回给浏览器即可
        return pay;
    }

}
