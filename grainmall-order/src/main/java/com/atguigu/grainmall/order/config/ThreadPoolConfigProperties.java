package com.atguigu.grainmall.order.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author shiyuan
 * @Date 2023/3/9 12:34
 * @Version 1.0
 * 线程池配置
 **/
@ConfigurationProperties(prefix = "grainmall.thread")
@Component
@Data
public class ThreadPoolConfigProperties {

    /**
     * 核心线程大小
     */
    private Integer coreSize;
    /**
     * 最大线程数
     */
    private Integer maxSize;
    /**
     * 空闲线程关闭回收时间(休眠时长)
     */
    private Integer keepAliveTime;
}
