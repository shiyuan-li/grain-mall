package com.atguigu.common.vaild;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @Author shiyuan
 * @Date 2023/1/27 16:02
 * @Version 1.0
 * 自定义校验注解
 **/
@Documented
@Constraint(validatedBy = {ListValueConstraintValidator.class}) // 指定校验器 不指定就需要在初始化时指定 将校验器与校验注解关联起来 可以同时指定多个校验器
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE}) // 再什么地方可以使用校验 属性-方法上等
@Retention(RetentionPolicy.RUNTIME) // 在何时进行校验 默认为运行时
public @interface ListValue {

    String message() default "{com.atguigu.common.vaild.ListValue.message}"; // com.atguigu.common.vaild.ListValue.message 为 取值的配置文件所在路径

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] values() default {};

}
