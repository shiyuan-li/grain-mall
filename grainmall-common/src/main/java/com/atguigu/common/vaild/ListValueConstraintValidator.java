package com.atguigu.common.vaild;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author shiyuan
 * @Date 2023/1/27 16:18
 * @Version 1.0
 * 自定义校验器  需要指定 要校验什么类型的 数据 泛型  <ListValue,Integer> 泛型指定 那哥注解 对应数据类型
 **/
public class ListValueConstraintValidator implements ConstraintValidator<ListValue,Integer> {

    private Set<Integer> set = new HashSet<>();

    // 初始化方法
    @Override
    public void initialize(ListValue constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        int[] values = constraintAnnotation.values();
        // 遍历 数据 添加到set中 使用set防止重复数据
        for (int value:values) {
            set.add(value);
        }
    }

    // 判断是否检验成功
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        // 判断是否存在 提交校验的数据值 包含就直接返回true 否则 false
        return set.contains(value);
    }
}
