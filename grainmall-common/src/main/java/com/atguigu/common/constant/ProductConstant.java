package com.atguigu.common.constant;

/**
 * @Author shiyuan
 * @Date 2023/2/7 16:07
 * @Version 1.0
 * 全局常量定义类
 **/
public class ProductConstant {

    /**
     * attr属性相关，枚举
     */
    public enum AttrEnum {
        // 规格参数 基本 属性Type
        ATTR_TYPE_BASE(1, "基本属性"),
        // 销售属性Type
        ATTR_TYPE_CALE(0, "销售属性");

        // 构造器
        AttrEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        private int code;
        private String msg;


        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    /**
     * status状态，枚举
     */
    public enum StatusEnum {
        // 商品新建
        NEW_SPU(0, "商品新建"),
        // 商品上架
        SPU_UP(1,"商品上架"),
        // 商品下架
        SPU_DOWN(2,"商品下架");

        private int code;
        private String msg;

        // 构造器
        StatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
        }
}
