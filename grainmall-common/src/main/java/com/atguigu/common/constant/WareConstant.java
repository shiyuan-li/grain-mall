package com.atguigu.common.constant;

/**
 * @Author shiyuan
 * @Date 2023/2/16 19:14
 * @Version 1.0
 * 库存常量
 **/
public class WareConstant {

    /**
     * 采购单状态，枚举
     */
    public enum PurchaseStatusEnum{
        // 新建状态
        CREATED(0,"新建"),
        // 已分配状态
        ASSIGNED(1,"已分配"),
        // 已领取状态
        RECEIVE(2,"已领取"),
        // 已完成状态
        FINISH(3,"已完成"),
        // 有异常状态
        HASERROR(4,"有异常");
        // 构造器
        PurchaseStatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }
        private int code;
        private String msg;


        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
    /**
     * 采购单状态，枚举
     */
    public enum PurchaseDetailStatusEnum{
        // 新建状态
        CREATED(0,"新建"),
        // 已分配状态
        ASSIGNED(1,"已分配"),
        // 已领取状态
        BUYING(2,"正在采购"),
        // 已完成状态
        FINISH(3,"已完成"),
        // 有异常状态
        HASERROR(4,"采购失败");
        // 构造器
        PurchaseDetailStatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }
        private int code;
        private String msg;


        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
