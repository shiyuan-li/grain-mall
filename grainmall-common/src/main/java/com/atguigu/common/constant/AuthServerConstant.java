package com.atguigu.common.constant;

/**
 * @Author shiyuan
 * @Date 2023/3/12 9:39
 * @Version 1.0
 * 验证码接口常量
 **/
public class AuthServerConstant {

    public static final String SMS_CODE_CACHE_PERFIX = "sms:code:";

    /**
     * 登陆成功的用户
     */
    public static final String LOGIN_USER = "loginUser";


}
