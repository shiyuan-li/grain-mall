package com.atguigu.common.constant;

/**
 * @Author shiyuan
 * @Date 2023/3/16 10:00
 * @Version 1.0
 * 购物车常量
 **/
public class CartConstant {

    /**
     * cookie键-名字
     */
    public static final String TEMP_USER_COOKIE_NAME = "user-key";

    /**
     * cookie作用时间
     */
    public static final int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30;

    /**
     * 购物车前缀
     */
    public final static String CART_PREFIX = "grainmall:cart:";
}
