package com.atguigu.common.to.mq;

import lombok.Data;

/**
 * @Author shiyuan
 * @Date 2023/3/23 13:27
 * @Version 1.0
 **/

@Data
public class StockLockedTo {

    /** 库存工作单的id **/
    private Long id;

    /** 工作单详情的所有信息 **/
    private StockDetailTo detailTo;
}
