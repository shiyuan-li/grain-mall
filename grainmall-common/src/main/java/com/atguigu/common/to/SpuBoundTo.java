package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author shiyuan
 * @Date 2023/2/16 12:53
 * @Version 1.0
 * spu积分信息
 **/
@Data
public class SpuBoundTo {

    private Long spuId;

    /**
     * 购物积分
     */
    private BigDecimal buyBounds;

    /**
     * 成长积分
     */
    private BigDecimal growBounds;

}
