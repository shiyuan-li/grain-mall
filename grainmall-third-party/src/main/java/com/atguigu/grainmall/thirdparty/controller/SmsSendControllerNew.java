package com.atguigu.grainmall.thirdparty.controller;

import com.atguigu.common.utils.R;
import com.atguigu.grainmall.thirdparty.component.SmsComponentNew;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author shiyuan
 * @Date 2023/3/11 10:35
 * @Version 1.0
 * 阿里云短信服务请求
 **/
@RestController
@RequestMapping(value = "/sms")
public class SmsSendControllerNew {
    @Resource
    private SmsComponentNew smsComponentNew;

    /**
     * 提供给别的服务进行调用
     * @param phone
     * @param code
     * @return
     */
    @GetMapping(value = "/sendCode")
    public R sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code) throws Exception {
        //发送验证码
        smsComponentNew.createClient(phone,code);

        return R.ok();
    }
}
