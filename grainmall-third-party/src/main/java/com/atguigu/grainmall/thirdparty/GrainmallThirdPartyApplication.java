package com.atguigu.grainmall.thirdparty;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author shiyuan
 */
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
public class GrainmallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallThirdPartyApplication.class, args);
    }

}
